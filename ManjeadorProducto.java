package org.jarriaza.manejadores;
import org.jarriaza.beans.Producto;
import java.util.ArrayList;
/**
	Clase que maneja el beans prodcuto
*/
public class ManjeadorProducto {
	private static ManjeadorProducto instancia;
	private ArrayList<Producto> listaProductos;
	/**
		Constructor privado que inicializa el ArrayList
	*/
	private ManjeadorProducto(){
		listaProductos=new ArrayList<Producto>();
		this.listaProductos.add(new Producto(1,"cereal",100,25));
	}
	/**
		Agrega un producto a la lista
		@param Producto producto
	*/
	public void agregarProducto(Producto producto){
		this.listaProductos.add(producto);
	}
	/**
		Elimina un producto de la lista
		@param Producto producto
	*/
	public void eliminarProducto(Producto producto){
		this.listaProductos.remove(producto);
	}
	/**
		Retorna la lista de productos
		@return ArrayList<Producto> listaProductos
	*/
	public ArrayList<Producto> mostrarProducto(){
			return this.listaProductos;
	}
	/**
		Busca un producto por su nombre
		@return Producto buscarProducto
	*/
	public Producto buscarProducto(String producto){
		for(Producto buscarProducto: listaProductos){
			if(buscarProducto.getProducto().equals(producto)){
				return buscarProducto;
			}
		}
		return null;
	}
	/**
		Instancia una sola vez
		@return ManjeadorProducto intancia
	*/
	public static ManjeadorProducto getInstancia(){
		if(instancia==null){
			instancia=new ManjeadorProducto();
		}
		return instancia;
	}
	/**
		Verifica si existe un producto
		@param String nombre
		@return boolean true or false
	*/
	public boolean verificarProducto(String nombre){
		for(Producto producto : listaProductos){
			if (producto.getProducto().equals(nombre)){
				return true;
			}
		}
		return false;
	}
	/**
		Busca el ultimo id y lo retorna
		@return int lastId
	*/
	public int lastId(){
		int id=0;
		for(Producto user:listaProductos){
			id=user.getIdProducto();
		}
		return id;
	}
}
