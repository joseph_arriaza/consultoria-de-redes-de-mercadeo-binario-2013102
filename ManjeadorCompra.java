package org.jarriaza.manejadores;
import org.jarriaza.beans.Compra;
import java.util.ArrayList;
/**
	Este clase maneja al beans COmpra
*/
public class ManjeadorCompra {
	private static ManjeadorCompra instancia;
	private ArrayList<Compra> historial;
	/**
		Constructor donde se instancia el ArrayList declarado
	*/
	private ManjeadorCompra(){
		historial=new ArrayList<Compra>();
	}
	/**
		Agrega una compra
		@param Compra compra
	*/
	public void agreagarCompra(Compra compra){
		this.historial.add(compra);
	}
	/**
		Muestra las compras
	*/
	public ArrayList<Compra> mostrarCompras(){
			return this.historial;
	}
	/**
		Regresa una compra especifica
		@param int idCompra
		@return Compra
	*/
	public Compra imprimiCompra(int idCompra){
		for(Compra compra:historial){
			if(compra.getIdCompra()==idCompra){
				return compra;
			}
		}
		return null;
	}
	/**
		Muestra las compras de productos especificos
		@param String producto
		@return String idCompras para luego separarlos y verificarlos
	*/
	public String verComprasProducto(String producto){
		String retorno=null;
		for(Compra compra:historial){
			if(compra.getProducto().equals(producto)){
				if(retorno==null){
					retorno=Integer.toString(compra.getIdCompra());
				}else{
					retorno=retorno+" "+compra.getIdCompra();
				}
			}
		}
		return retorno;
	}
	/**
		Muestra las compras de un usuario
		@param String nickname
		@return String idCompras id de las compras del usuario
	*/
	public String verMisCompras(String nickname){
		String retorno=null;
		for(Compra compra:historial){
			if(compra.getNickname().equals(nickname)){
				if(retorno==null){
					retorno=Integer.toString(compra.getIdCompra());
				}else{
					retorno=retorno+" "+compra.getIdCompra();
				}				
			}
		}
		return retorno;
	}
	/**
		Metodo que instancia solo una vez, patron Singleton
	*/
	public static ManjeadorCompra getInstancia(){
		if(instancia==null){
			instancia=new ManjeadorCompra();
		}
		return instancia;
	}
	/**
		Con este metodo se obtiene el ultimo id de la lista
	*/
	public int lastId(){
		int id=0;
		for(Compra compra:historial){
			id=compra.getIdCompra();
		}
		return id;
	}
}
