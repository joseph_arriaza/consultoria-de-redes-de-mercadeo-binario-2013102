package org.jarriaza.app;
import org.jarriaza.utilidad.Comandos;
import org.jarriaza.utilidad.Editar;
import org.jarriaza.utilidad.LeerDato;
import org.jarriaza.utilidad.ShowMe;
import org.jarriaza.utilidad.Fecha;
import org.jarriaza.utilidad.Tarjeta;
import org.jarriaza.utilidad.Fecha;
import org.jarriaza.utilidad.Decodificador;
import org.jarriaza.utilidad.ComandosProductos;
import org.jarriaza.utilidad.RecorrerDownline;
import org.jarriaza.beans.Usuario;
import org.jarriaza.beans.Downline;
import org.jarriaza.beans.Compra;
import org.jarriaza.beans.Oferta;
import org.jarriaza.manejadores.ManjeadorUsuario;
import org.jarriaza.manejadores.ManjeadorDownline;
import org.jarriaza.manejadores.ManjeadorProducto;
import org.jarriaza.manejadores.ManjeadorCompra;
import org.jarriaza.manejadores.ManjeadorOferta;
import java.util.HashMap;
import java.util.ArrayList;
/**
	Clase AppMiembro esta clase tiene las funciones del administrador impelementado la interfaz y extendiendose a la clase abstracta para el uso de sus metodos
*/
public class AppMiembro extends AbstractAppRol implements Comandos{
	ArrayList<Compra> listaCompras= new ArrayList<Compra>();
	/**
		Con este metodo se agrega un escuchador para que pueda realizar las funciones
		@param Decodificador decodificador
	*/
	public AppMiembro(Decodificador decodificador){
		decodificador.addActionListener(this);
		super.setDecodificador(decodificador);
	}
	/**
		Con este metodo se agrega un usuario
		@param Downline downline es el usuario a agregar.
		@param Usuario usuario es el usuario a agregar para login
		@param int idDownline es el idDownline del que lo agrega para actualizar sus datos de agregados
		@param int idDown es el id de debajo de quien va
	*/
	public void agregarDownline(Downline downline, Usuario usuario, int idDownline, int idDown){
		ManjeadorDownline.getInstanciar().agregarDownline(downline);
		ManjeadorUsuario.getInstanciar().agregarUsuario(usuario);
		int posicion=ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getIdDownline()-1;
		if(ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).getPersonasAgregadas()!=null){
			ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).setPersonasAgregadas(ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).getPersonasAgregadas()+" "+idDown);
		}else{
			ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).setPersonasAgregadas(Integer.toString(idDown));
		}
		System.out.println("	Downline agregado correctamente.");
	}
	/**
		Este metodo agrega una compra
		@param Compra compra es la compra a agregar
		@param int posicion es la posicion del usuario a la que se le agregar la compra
		@int consumo es el consumo del articulo para ver si se vuelve true o no
	*/
	public void agregarCompra(Compra compra, int posicion, int consumo){
		String fechaHoy=Fecha.fechaHoy();
		Oferta oferta=ManjeadorOferta.getInstancia().verOferta(compra.getProducto(),fechaHoy);	
		if(oferta!=null){
			int ofertaPrecio=consumo;
			if(oferta.getOferta().equals("2*1")){
				ManjeadorCompra.getInstancia().agreagarCompra(compra);
			}else{
				ofertaPrecio=Integer.parseInt(oferta.getOferta());
				ofertaPrecio=(consumo*ofertaPrecio)/100;
				consumo=ofertaPrecio;
				System.out.println("	Producto rebajado al "+ofertaPrecio+"% precio a pagar "+consumo);
			}
			compra.setConsumo(ofertaPrecio);
			ManjeadorCompra.getInstancia().agreagarCompra(compra);
			int poss=ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()).getIdDownline()-1;
			ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).setConsumo(ManjeadorDownline.getInstanciar().obtenerLista().get(poss).getConsumo()+consumo);
			if(ManjeadorDownline.getInstanciar().obtenerLista().get(poss).getConsumo()>=100){
				ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).setActivo(true);
			}
		}else{
			ManjeadorCompra.getInstancia().agreagarCompra(compra);
			int poss=ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()).getIdDownline()-1;
			ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).setConsumo(ManjeadorDownline.getInstanciar().obtenerLista().get(poss).getConsumo()+consumo);
			if(ManjeadorDownline.getInstanciar().obtenerLista().get(poss).getConsumo()>=100){
				ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).setActivo(true);
			}
		}
		System.out.println("	Compra agregada satisfactoriamente.");
	}
	/**
		Muestra el dinero de la pierna derecha
		@param Downline downline es el downline al que se le buscara el dinero
		@param int value Es el dinero que ya lleva y se ira sumando
		@param HashMap<String,String> objeto
	*/
	public static void mostrarPiernaRight(Downline downline, int value, HashMap<String,String> objeto){
		int valor=value;		
		if(downline.getRight()!=null){
			Downline right=ManjeadorDownline.getInstanciar().buscarDown(downline.getRight());
			if(right.getActivo()==true){
				valor=valor+right.getConsumo();
			}else{
				System.out.println("aqui esta");
				valor=valor;
			}
			if(right.getLeft()!=null){
				Downline left=ManjeadorDownline.getInstanciar().buscarDown(right.getLeft());
				if(left.getActivo()==true){
					valor=valor+left.getConsumo();					
				}else{
					valor=valor;
				}
			}
			mostrarPiernaRight(right,valor,objeto);	
		}else{
			objeto.put("derecha",Integer.toString(valor));
		}
	}
	/**
		Muestra el dinero de la pierna izquierda
		@param Downline downline es el downline al que se le buscara el dinero
		@param int value Es el dinero que ya lleva y se ira sumando
		@param HashMap<String,String> objeto
	*/
	public static void mostrarPiernaLeft(Downline downline, int value, HashMap<String,String> objeto){
		int valor=value;
		if(downline.getLeft()!=null){
			Downline left=ManjeadorDownline.getInstanciar().buscarDown(downline.getLeft());
			if(left.getActivo()==true){
				valor=valor+left.getConsumo();
			}else{
				valor=valor;
			}
			if(left.getRight()!=null){
				Downline right=ManjeadorDownline.getInstanciar().buscarDown(left.getRight());
				if(right.getActivo()==true){
					mostrarPiernaRight(right,valor,objeto);
					if(objeto.get("derecha")!=null){
						valor=Integer.parseInt(objeto.get("derecha"));						
					}
					valor=valor+right.getConsumo();					
				}else{
					valor=valor;
				}
			}
			mostrarPiernaLeft(left,valor,objeto);	
		}else{
			objeto.put("izquierda",Integer.toString(valor));
		}
	}
	/**
		Muestra el downline de la pierna izquierda
		@param Downline downline es el downline al que se le buscara el dinero
		@param int value Es el dinero que ya lleva y se ira sumando
		@param HashMap<String,String> objeto
	*/
	public void mostrarDownlineLeft(int myId){
		if(ManjeadorDownline.getInstanciar().buscarDownlineUser(myId).getLeft()!=null){
				System.out.println("	Downlines lado izquierdo");
				System.out.println("_____________________________________________________________________");
				System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownlineUser(myId).getLeft()).getNombre());
				System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownlineUser(myId).getLeft()).getConsumo());
			}else{
				System.out.println("_____________________________________________________________________");
				System.out.println("	No tienes ningun downline del lado izquierdo.");
			}
		System.out.println("_____________________________________________________________________");
	}
	/**
		Muestra el downline de la pierna derecha
		@param Downline downline es el downline al que se le buscara el dinero
		@param int value Es el dinero que ya lleva y se ira sumando
		@param HashMap<String,String> objeto
	*/
	public void mostrarDownlineRight(int myIdUser){
		if(ManjeadorDownline.getInstanciar().buscarDownlineUser(myIdUser).getRight()!=null){
			System.out.println("	Downlines lado derecho");
			System.out.println("_____________________________________________________________________");
			System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownlineUser(myIdUser).getRight()).getNombre());
			System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownlineUser(myIdUser).getRight()).getConsumo());
		}else{
			System.out.println("	No tienes ningun downline del lado derecho.");
		}
		System.out.println("_____________________________________________________________________");
	}
	/**
		Con este metodo se reconoce el comando y si no aparece lo manda a la clase abstracta
		@param String accion
		@param HashMap<String,String> objeto
	*/
	public void avisarAccionar(String accion, HashMap<String, String> objeto){
		boolean verificar=false;
		switch(accion){			
			case "list downlines left":
				int myIdUser=ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario();
				this.mostrarDownlineLeft(myIdUser);
			break;
			case "list downlines right":
				int myId=ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario();
				this.mostrarDownlineRight(myId);
			break;			
			case "edit me":
				Editar.editarUsuario(accion, objeto);
				break;
			case "add downline":
				if(objeto.get("nickname")!=null && objeto.get("pass")!=null && objeto.get("nombre")!=null && objeto.get("apellido")!=null && objeto.get("edad")!=null && objeto.get("email")!=null && objeto.get("direccion")!=null && objeto.get("pierna")!=null && objeto.get("down")!=null && objeto.get("product")!=null&& objeto.get("tarjeta")!=null){
					if(objeto.get("pierna").equals("left") || objeto.get("pierna").equals("right")){
						int idUser;						
						if(ManjeadorProducto.getInstancia().verificarProducto(objeto.get("product"))==true){
							if(ComandosProductos.isNumeric(objeto.get("down"))){							
								if(AppAdmin.isEmail(objeto.get("email"))){						
									if(ComandosProductos.isNumeric(objeto.get("edad"))){
										int edad=Integer.parseInt(objeto.get("edad"));
										if(edad>=18 && edad<=90){								
											if(Tarjeta.esNumeroLargo(objeto.get("tarjeta"))){	
												if(objeto.get("tarjeta").length()==16){
													if(Tarjeta.comprobarTarjeta(objeto.get("tarjeta"))==true){
														System.out.println("Esta seguro que desea agreagar a un downline con las siguientes propiedades?");
														System.out.println("Nickmame:    "+objeto.get("nickname"));
														System.out.println("Pass:        "+objeto.get("pass"));
														System.out.println("Nombre:      "+objeto.get("nombre"));
														System.out.println("Apellido:    "+objeto.get("apellido"));
														System.out.println("Edad:        "+objeto.get("edad"));
														System.out.println("Email:       "+objeto.get("email"));
														System.out.println("Direccion:   "+objeto.get("direccion"));
														System.out.println("Pierna:	     "+objeto.get("pierna"));
														System.out.println("Down:	     "+objeto.get("down"));
														System.out.println("Producto:    "+objeto.get("product"));
														System.out.println("Tarjeta:     "+objeto.get("tarjeta"));
														System.out.println("Si o No");
														String op=LeerDato.getInstancia().leer();
														if(op.equalsIgnoreCase("Si")){	
															long tarjeta=Long.parseLong(objeto.get("tarjeta"));
															int id=ManjeadorUsuario.getInstanciar().lastId()+1;													
															int idDown=ManjeadorDownline.getInstanciar().lastId()+1;
															int idDownUp=Integer.parseInt(objeto.get("down"));
															int idDownLine=ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()).getIdDownline();
															if(ManjeadorProducto.getInstancia().verificarProducto(objeto.get("product"))==true){
																if(ManjeadorDownline.getInstanciar().verificarDownline(idDownUp)==true){
																	if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownUp),idDownUp)==true){										
																		String nombreDown=ManjeadorDownline.getInstanciar().buscarDownline(idDownUp).getNickname();
																		if(ManjeadorUsuario.getInstanciar().buscarUsuario(nombreDown).getRango().equals("miembro")){
																			boolean aceptar=ManjeadorUsuario.getInstanciar().verificarUser(objeto.get("nickname"));
																		if(aceptar==false){
																			idUser=Integer.parseInt(objeto.get("down"));																			
																			if(idDownLine<=idUser){
																				if(objeto.get("pierna").equals("left")){
																					if(ManjeadorDownline.getInstanciar().buscarDownline(idUser).getLeft()==null){
																						verificar = true;																	
																					}
																				}
																				if(objeto.get("pierna").equals("right")){
																					if(ManjeadorDownline.getInstanciar().buscarDownline(idUser).getRight()==null){
																						verificar = true;
																					}
																				}
																			}
																			if(verificar==true){																
																				Usuario usuario=new Usuario(id,objeto.get("nickname"),objeto.get("pass"),objeto.get("nombre"),objeto.get("apellido"),"miembro",edad,objeto.get("email"),objeto.get("direccion"));
																				int consumo=ManjeadorProducto.getInstancia().buscarProducto(objeto.get("product")).getPrecio();
																				if(ManjeadorProducto.getInstancia().buscarProducto(objeto.get("product")).getPrecio()>=100){								
																					Downline downline=new Downline(idDown,id,objeto.get("nickname"),objeto.get("nombre"),idDownUp,objeto.get("product"),consumo,true,tarjeta,ManjeadorUsuario.getInstanciar().usuarioLogeado().getNickname());
																					this.agregarDownline(downline,usuario,idDownLine,idDown);
																					int posicion=ManjeadorDownline.getInstanciar().buscarDownline(idUser).getIdDownline()-1;
																					if(objeto.get("pierna").equals("left")){																	
																						ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).setLeft(objeto.get("nickname"));
																					}else{
																						ManjeadorDownline.getInstanciar().obtenerLista().get(posicion).setRight(objeto.get("nickname"));
																					}
																				}else{
																					System.out.println("	El downline debe consumir mas de 100.");
																				}
																			}else{
																				System.out.println("	Esta pierna ya esta en uso.");
																			}
																		}else{
																			System.out.println("	El downline ya existe.");
																		}
																	}else{
																		System.out.println("	El downline solo puede ser un usuario.");
																	}
																}else{
																	System.out.println("	No puedes agregar un downline aqui.");
																}
															}else{
																System.out.println("	El downline no existe.");
															}															
														}else{
															System.out.println("	No puedes agregar un downline aqui.");
														}
													}else{
														System.out.println("	No se ha agregado al usuario.");
													}
												}else{
													System.out.println("	Ingresa una tarjeta de credito valida.");
												}												
											}else{
												System.out.println("	Tu tarjeta debe de tener 16 digitos.");
											}
										}else{
											System.out.println("	La tarjeta debe de ser tipo numerico.");
										}
									}else{
											System.out.println("	El usuario debe ser mayor de edad y debe de tener una edad razonable.");
									}	
								}else{
									System.out.println("	La edad debe de ser tipo numerico.");
								}
							}else{
								System.out.println("	Ingresa un email valido.");
							}
						}else{
							System.out.println("	El idDownline debe de ser tipo numerico.");
						}
					}else{
							System.out.println("	El producto no existe.");
					}
				}else{
					System.out.println("	La pierna solo puede ser right o left.");
				}
			}else{
					if(objeto.get("nickname")==null){
						System.out.println("	Debe de ingresar un nickname, nickname=nickname");
					}
					if(objeto.get("pass")==null){
						System.out.println("	Debe ingresar una password, pass=pass");
					}
					if(objeto.get("nombre")==null){
						System.out.println("	Debe ingresar un nombre, nombre=nombre");
					}				
					if(objeto.get("apellido")==null){
						System.out.println("	Debe ingresar un apellido, apellido=apellido");
					}
					if(objeto.get("edad")==null){
						System.out.println("	Debe ingresar una edad, edad=edad");
					}
					if(objeto.get("email")==null){
						System.out.println("	Debe ingresar un email, email=email");
					}
					if(objeto.get("direccion")==null){
						System.out.println("	Debe ingresar una direccion, direccion=direccion");
					}
					if(objeto.get("tarjeta")==null){
						System.out.println("	Debe ingresar una tarjeta de creidto, tarjeta=tarjeta");
					}
					if(objeto.get("down")==null){
						System.out.println("	Debe ingresar un upline al que pertenece, down \"idDownline\"");
					}
					if(objeto.get("product")==null){
						System.out.println("	Debe ingresar un producto, \"product\"");
					}
					if(objeto.get("pierna")==null){
						System.out.println("	Debe ingresar una pierna a la que pertenece, \"pierna\"");
					}					
				}
				break;
			case "buy product":
				if(objeto.get("producto")!=null){
					if(ManjeadorProducto.getInstancia().verificarProducto(objeto.get("producto"))==true){						
						System.out.println("Agregara una compra con las siguientes caracteristicas: ");
						System.out.println("Producto:	   "+objeto.get("producto"));
						System.out.println("Precio:		   "+ManjeadorProducto.getInstancia().buscarProducto(objeto.get("producto")).getPrecio());
						if(objeto.get("tarjeta")!=null){	
							System.out.println("Tarjeta:	   "+objeto.get("tarjeta"));
						}
						System.out.println("Fecha Pedido:      "+Fecha.fechaHoy());
						System.out.println("Fecha Entrega:     "+Fecha.fechaEntrega());
						System.out.println("");
						System.out.println("	Desea agregar la compra, Si o No?");
						//buy product "Cereal" 123
						String op=LeerDato.getInstancia().leer();							
						if(op.equalsIgnoreCase("Si")){
							int idCompra=ManjeadorCompra.getInstancia().lastId()+1;								
							int posicion=ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()).getIdDownline()-1;
							Compra compra= new Compra(idCompra,objeto.get("producto"),ManjeadorUsuario.getInstanciar().usuarioLogeado().getNickname(),Fecha.fechaHoy(),Fecha.fechaEntrega(),ManjeadorProducto.getInstancia().buscarProducto(objeto.get("producto")).getPrecio());
							this.agregarCompra(compra, posicion, ManjeadorProducto.getInstancia().buscarProducto(objeto.get("producto")).getPrecio());
						}else{
							System.out.println("	No se ha agregado la compra.");
						}						
					}else{
						System.out.println("	El producto no existe.");
					}
				}else{
					System.out.println("	La sintaxis del comando es buy product \"producto\" [tarjeta]");
				}				
				break;
			case "list downlines":
				int idUserMy=ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario();
				this.mostrarDownlineLeft(idUserMy);
				this.mostrarDownlineRight(idUserMy);				
			break;
			case "show history":
				int idUsuario=ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario();
				if(ManjeadorDownline.getInstanciar().buscarDownlineUser(idUsuario).getPersonasAgregadas()!=null){					
					String historialAgregados[]=ManjeadorDownline.getInstanciar().buscarDownlineUser(idUsuario).getPersonasAgregadas().split(" ");
					System.out.println("____________________________________________");
					for(String recorrer:historialAgregados){
						int idUser=Integer.parseInt(recorrer);
						int poss=idUser-1;
						System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().obtenerLista().get(poss).getNombre());
						System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().obtenerLista().get(poss).getConsumo());
					}
					System.out.println("____________________________________________");
				}else{
					System.out.println("	No has agregado a ningun downline.");
				}
			break;			
			case "show history buy":
				if(ManjeadorCompra.getInstancia().verMisCompras(ManjeadorUsuario.getInstanciar().usuarioLogeado().getNickname())!=null){
					String compras[]=ManjeadorCompra.getInstancia().verMisCompras(ManjeadorUsuario.getInstanciar().usuarioLogeado().getNickname()).split(" ");
					System.out.println("____________________________________________");
					for(String compra:compras){
						int idCompra=Integer.parseInt(compra);
						Compra impCompras=ManjeadorCompra.getInstancia().imprimiCompra(idCompra);
						System.out.println("Nickname     : 		"+impCompras.getNickname());
						System.out.println("Producto     :		"+impCompras.getProducto());
						System.out.println("Fecha Pedido :		"+impCompras.getFechaPedido());
						System.out.println("Fecha Entrega:		"+impCompras.getFechaEntrega());
						System.out.println("Consumo      :		"+impCompras.getConsumo());
						System.out.println("____________________________________________");
					}
				}else{
					System.out.println("	No tienes compras.");
				}
				break;
			case "logout":
				super.setConnected(false);
				break;
			default:
				super.avisarAccionar(accion, objeto);
				break;
		}
	}
}