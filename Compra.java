package org.jarriaza.beans;
/**
	Beans de compra
*/
public class Compra {
	private int idCompra;
	private String producto;
	private String nickname;
	private String fechaPedido;
	private String fechaEntrega;
	private int consumo;
	public int getConsumo(){
		return consumo;
	}
	public void setConsumo(int consumo){
		this.consumo=consumo;
	}
	/**
		Muestra la fecha de Compra
	*/
	public String getFechaPedido() {
		return fechaPedido;
	}
	/**
		Recibe la fecha de Compra
		@param String fechaPedido
	*/
	public void setFechaPedido(String fechaPedido) {
		this.fechaPedido = fechaPedido;
	}
	/**
		Muestra el id
	*/
	public int getIdCompra() {
		return idCompra;
	}
	/**
		Recibe el id
		@param int idCompra
	*/
	public void setIdCompra(int idCompra) {
		this.idCompra = idCompra;
	}
	/**
		Muestra el producto de compra
	*/
	public String getProducto() {
		return producto;
	}
	/**
		Muestra la fecha de entrega
	*/
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	/**
		Recibe la fecha de entrega
		@param String fechaEntrega
	*/
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	/**
		Recibe el producto
		@param String producto
	*/
	public void setProducto(String producto) {
		this.producto = producto;
	}
	/**
		Muestra el nickname
	*/
	public String getNickname() {
		return nickname;
	}
	/**
		Recibe el nickname
		@param String nickname
	*/
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
		Constructo vacio
	*/
	public Compra() {
		super();
	}
	/**
		Constructor lleno
		@param int idCompra
		@param String producto
		@param String nickname
		@param String fechaPedido
		@param String 	fechaEntrega
		@param int consumo
	*/
	public Compra(int idCompra, String producto, String nickname, String fechaPedido, String fechaEntrega, int consumo) {
		super();
		this.idCompra = idCompra;
		this.producto = producto;
		this.nickname = nickname;
		this.fechaPedido = fechaPedido;
		this.fechaEntrega = fechaEntrega;
		this.consumo = consumo;
	}	
}