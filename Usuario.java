package org.jarriaza.beans;
/**
	Beans de usuario
*/
public class Usuario{
		private int idUsuario;
		private String nickname;
		private String pass;
		private String nombre;
		private String apellido;
		private String rango;
		private int edad;
		private String email;
		private String direccion;
		private String upline;
		private int consumoMensual;
		private int personasAgregadas;
		private String piernaPertenece;
		private String personaAgrega;
		private String left;
		private String right;
		/**
			Se devuelve las personas agregadas
			@return int personasAgregadas
		*/
		public int getPersonasAgregadas() {
			return personasAgregadas;
		}
		/**
			Se devuelve la persona del lado izquierdo
			@return String left
		*/
		public String getLeft() {
			return left;
		}
		/**
			Ingresa la persona en el lado izquierdo
			@param String left
		*/
		public void setLeft(String left) {
			this.left = left;
		}
		/**
			Se devuelve la persona del lado derecho
			@return String right
		*/
		public String getRight() {
			return right;
		}
		/**
			Ingresa la persona en el lado derecho
			@param String right
		*/
		public void setRight(String right) {
			this.right = right;
		}
		/**
			Ingresa la cantidad de personas agregadas
			@param int personasAgregadas
		*/
		public void setPersonasAgregadas(int personasAgregadas) {
			this.personasAgregadas = personasAgregadas;
		}
		/**
			Se devuelve la pierna a la que perteneces
			@return String piernaPertenece
		*/
		public String getPiernaPertenece() {
			return piernaPertenece;
		}
		/**
			Ingresa la pierna a la que pertenece
			@param String piernaPertenece
		*/
		public void setPiernaPertenece(String piernaPertenece) {
			this.piernaPertenece = piernaPertenece;
		}
		/**
			Devuelvo la persona que lo agrego
			@return String personaAgrega
		*/
		public String getPersonaAgrega() {
			return personaAgrega;
		}
		/**
			Ingresa la persona que agrega
			@param String personaAgrega
		*/
		public void setPersonaAgrega(String personaAgrega) {
			this.personaAgrega = personaAgrega;
		}
		/**
			Obtiene el id 
			@return int idUsuario
		*/
		public int getIdUsuario() {
			return idUsuario;
		}
		/**
			Ingresa el id
			@param int idUsuario
		*/
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		/**
			Devuelve el nickname
			@return String nickname
		*/
		public String getNickname() {
			return nickname;
		}
		/**
			Ingresa el nickname
			@param String nickname
		*/
		public void setNickname(String nickname) {
			this.nickname = nickname;
		}
		/**
			Devuelve la password
			@return String pass
		*/
		public String getPass() {
			return pass;
		}
		/**
			Ingresa la password
			@param String password
		*/
		public void setPass(String pass) {
			this.pass = pass;
		}
		/**
			Devuelve el nombre
			@return String nombre
		*/
		public String getNombre() {
			return nombre;
		}
		/**
			Ingresa el nombre
			@param String nombre
		*/
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		/**
			Devuelve el apellido
			@return String apellido
		*/
		public String getApellido() {
			return apellido;
		}
		/**
			Ingresa el apellido
			@param String apellido
		*/
		public void setApellido(String apellido) {
			this.apellido = apellido;
		}
		/**
			Devuelve el rol
			@return String rango
		*/
		public String getRango() {
			return rango;
		}
		/**
			Ingresa el rango
			@param String rango
		*/
		public void setRango(String rango) {
			this.rango = rango;
		}
		/**
			Devuelve la edad
			@return int edad
		*/
		public int getEdad() {
			return edad;
		}
		/**
			Ingresa la edad
			@param int edad
		*/
		public void setEdad(int edad) {
			this.edad = edad;
		}
		/**
			Regresa el email
			@return String email
		*/
		public String getEmail() {
			return email;
		}
		/**
			Ingresa el email
			@param String personaAgrega
		*/
		public void setEmail(String email) {
			this.email = email;
		}
		/**
			Devuelve la direccion
			@return String direccion
		*/
		public String getDireccion() {
			return direccion;
		}
		/**
			Ingresa la direccion
			@param String direccion
		*/
		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}
		/**
			Devueve el upline
			@return String upline
		*/
		public String getUpline() {
			return upline;
		}
		/**
			Ingresa el upline
			@param String upline
		*/
		public void setUpline(String upline) {
			this.upline = upline;
		}
		/**
			Devulve el consumo mensual
			@return String consumoMensual
		*/
		public int getConsumoMensual() {
			return consumoMensual;
		}
		/**
			Ingresa el consumo
			@param String consumoMensual
		*/
		public void setConsumoMensual(int consumoMensual) {
			this.consumoMensual = consumoMensual;
		}	
		/**
			Constructor vacio
		*/
		public Usuario(){
			super();
		}
		/**
			Constructor para admin
			@param int idUsuario
			@param String nickname
			@param String pass
			@param String nombre
			@param String apellido
			@param String rango
			@param int edad
			@param String email
			@param String direccion
		*/
		public Usuario(int idUsuario,String nickname,String pass,String nombre,String apellido,String rango,int edad,String email,String direccion){
			setIdUsuario(idUsuario);
			setNickname(nickname);
			setPass(pass);
			setNombre(nombre);
			setApellido(apellido);
			setRango(rango);
			setEdad(edad);
			setEmail(email);
			setDireccion(direccion);
		}
		/**
			Constructor para usuario
			@param int idUsuario
			@param String nickname
			@param String pass
			@param String nombre
			@param String apellido
			@param String rango
			@param int edad
			@param String email
			@param String direccion
			@param int consumoMensual
			@param int personasAgregas
			@param String piernaPertenece
			@param String personaAgrega
			@param String left
			@param String right
		*/
		public Usuario(int idUsuario,String nickname,String pass,String nombre,String apellido,String rango,int edad,String email,String direccion,int consumoMensual,int personasAgregadas,String piernaPertenece,String personaAgrega,String left,String right){
			setIdUsuario(idUsuario);
			setNickname(nickname);
			setPass(pass);
			setNombre(nombre);
			setApellido(apellido);
			setRango(rango);
			setEdad(edad);
			setEmail(email);
			setDireccion(direccion);
			setConsumoMensual(consumoMensual);
			setPersonasAgregadas(personasAgregadas);
			setPiernaPertenece(piernaPertenece);
			setPersonaAgrega(personaAgrega);
			setLeft(left);
			setRight(right);
		}
		
}
