package org.jarriaza.manejadores;
import org.jarriaza.beans.Downline;
import java.util.ArrayList;
/**
	Clase que maneja el beans downline
*/
public class ManjeadorDownline{
	private static ManjeadorDownline instanciar;
	private ArrayList<Downline> downlines;
	/**
		Constructor privado que inicializa el ArrayList
	*/	
	private ManjeadorDownline(){
		downlines=new ArrayList<Downline>();
		this.downlines.add(new Downline(1,2,"user","Juan",0,"",0,false,1,"user"));
	}
	/**
		Agrega un usuario
		@param Downline donwline
	*/	
	public void agregarDownline(Downline downline){
		this.downlines.add(downline);
	}
	/**
		Retorna la lista de dowlines
		@return ArrayList<Downline> downlines
	*/
	public ArrayList<Downline> obtenerLista(){
		return this.downlines;
	}
	/**
		Retorna el ultimo id
		@return int lastId
	*/
	public int lastId(){
		int id=0;
		for(Downline downline:downlines){
			id=downline.getIdDownline();
		}
		return id;
	}
	/**
		Verifica si el downline existe con el id
		@param int id
		@return boolean true or false
	*/
	public boolean verificarDownline(int id){
		for(Downline down : downlines){
			if (down.getIdDownline()==id){
				return true;
			}
		}
		return false;
	}
	/**
		Busca un downline con su nickname
		@param String down
		@return Downline buscaDown
	*/
	public Downline buscarDown(String down){
		for(Downline buscaDown: downlines){
			if(buscaDown.getNickname().equals(down)){
				return buscaDown;
			}
		}
		return null;
	}
	/**
		Busca un downline con su id
		@param int down
		@return Downline buscaDown
	*/
	public Downline buscarDownline(int down){
		for(Downline buscaDown: downlines){
			if(buscaDown.getIdDownline()==down){
				return buscaDown;
			}
		}
		return null;
	}
	/**
		Busca un downline con el id del usuario
		@param int down
		@return Dwonline buscaDonw
	*/
	public Downline buscarDownlineUser(int down){
		for(Downline buscaDown: downlines){
			if(buscaDown.getIdUsuario()==down){
				return buscaDown;
			}
		}
		return null;
	}
	/**
		Verifica la instancia
		@return ManjeadorDownline instanciar
	*/
	public static ManjeadorDownline getInstanciar(){
		if(instanciar==null){
			instanciar=new ManjeadorDownline();
		}
		return instanciar;
	}
}