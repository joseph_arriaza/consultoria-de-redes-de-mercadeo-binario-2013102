	package org.jarriaza.utilidad;
import java.util.HashMap;

import org.jarriaza.manejadores.ManjeadorUsuario;
/**
	Esta clase depura algunos comandos
*/
public class DepurarComillas {
	/**
		Depura algunos comandos que necesitan o no la comilla
		@param String comando
		@param HashMap<String,String> objeto
	*/
	public static String depurar(String comando, HashMap<String,String> objeto){
		String[] comandos = comando.split(" ");
		String accion = "";
		if(comandos.length>=2){
			accion=comandos[0]+" "+comandos[1];
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){				
				if(accion.equalsIgnoreCase("buy product")){
					if(comandos.length==4||comandos.length==5||comandos.length==3){						
						try{
							Long.parseLong(comandos[comandos.length-1]);
							if(comandos.length>=3){
								char toCharArrayProducto[]=comandos[2].toCharArray();
								if(toCharArrayProducto[0]=='"'){
									if(toCharArrayProducto[toCharArrayProducto.length-1]=='"'){
										String [] quitarComillaProducto=comandos[2].split("\"");
										if(quitarComillaProducto.length==2){
											if(Tarjeta.comprobarTarjeta(comandos[comandos.length-1])){
												objeto.put("producto",quitarComillaProducto[1]);
												objeto.put("tarjeta",comandos[comandos.length-1]);
											}else{
												accion="sintaxis";
												System.out.println("	Debes ingresar una tarjeta de credito valida.");
											}
										}else{
											accion="sintaxis";
											System.out.println("	Debes ingresar algo en las comillas.");
										}
									}else{
										if(comandos.length>=4 && comandos.length<6){
											String valor=comandos[2]+" "+comandos[3];
											char toCharArrayProducto2[]=valor.toCharArray();
											if(toCharArrayProducto2[toCharArrayProducto2.length-1]=='"'){
												String [] quitarComillaProducto2=valor.split("\"");
												if(quitarComillaProducto2.length==2){
													if(Tarjeta.comprobarTarjeta(comandos[comandos.length-1])){
														objeto.put("producto",quitarComillaProducto2[1]);
														objeto.put("tarjeta",comandos[comandos.length-1]);
													}else{
														accion="sintaxis";
														System.out.println("	Debes ingresar una tarjeta de credito valida.");
													}
												}else{
													accion="sintaxis";
													System.out.println("	Debes ingresar algo en las comillas.");
												}
											}
										}else{
											accion="sintaxis";
										}
									}	
								}else{
									accion="sintaxis";
								}
							}
						}catch(NumberFormatException nfe){
							if(comandos.length>=3){
								char toCharArrayProducto[]=comandos[2].toCharArray();
								if(toCharArrayProducto[0]=='"'){
									if(toCharArrayProducto[toCharArrayProducto.length-1]=='"'){
										String [] quitarComillaProducto=comandos[2].split("\"");
										if(quitarComillaProducto.length==2){
											objeto.put("producto",quitarComillaProducto[1]);
										}else{
											accion="sintaxis";
											System.out.println("	Debes ingresar algo en las comillas.");
										}
									}else{
										if(comandos.length==4){
											String valor=comandos[2]+" "+comandos[3];
											char toCharArrayProducto2[]=valor.toCharArray();
											if(toCharArrayProducto2[toCharArrayProducto2.length-1]=='"'){
												String [] quitarComillaProducto2=valor.split("\"");
												if(quitarComillaProducto2.length==2){
													objeto.put("producto",quitarComillaProducto2[1]);
												}else{
													accion="sintaxis";
													System.out.println("	Debes ingresar algo en las comillas.");
												}
											}
										}else{
											accion="sintaxis";
										}
									}	
								}else{
									accion="sintaxis";
								}
							}
						}
					}						
				}
			}
		}

			if(comandos.length>=2){
				accion=comandos[0]+" "+comandos[1];
				if(accion.equalsIgnoreCase("list user")||accion.equalsIgnoreCase("list offert")||accion.equalsIgnoreCase("list products")||accion.equalsIgnoreCase("show history")||accion.equalsIgnoreCase("list downlines")||accion.equalsIgnoreCase("show sales")){
					if(comandos.length>=3){
						if(accion.equalsIgnoreCase("show sales")){
							if(comandos.length==3){							
								char valor[]=comandos[2].toCharArray();
								if(valor[0]=='"'){
									for(int cont=2;cont<valor.length;cont++){
										if(valor[cont]=='"'){
											String verComilla[]=comandos[2].split("\"");
											accion=comandos[0]+" "+comandos[1];
											objeto.put("producto",verComilla[1].toLowerCase());
										}
									}
								}else{
									accion="sintaxis";
								}
							}else{
								if(comandos.length==4){						
									String comandoValor=comandos[2]+" "+comandos[3];
									char valor[]=comandoValor.toCharArray();
									if(valor[0]=='"'){
										for(int cont=2;cont<valor.length;cont++){
											if(valor[cont]=='"'){
												String verComilla[]=comandoValor.split("\"");
												accion=comandos[0]+" "+comandos[1];
												objeto.put("producto",verComilla[1].toLowerCase());
											}
										}
									}else{
										accion="sintaxis";
									}
								}else{
									if(comandos.length>4){
										accion="sintaxis";
									}
								}
							}							

						}else{
							accion="";
						}						
						if(comandos[2].equalsIgnoreCase("buy")){
							accion=comandos[0]+" "+comandos[1]+" "+comandos[2];
							if(comandos.length>=4){
								if(comandos[3].equals("downline")){									
									if(comandos.length==5){
										accion="show history buy downline";
										char[] verificarComilla=comandos[4].toCharArray();
										if(verificarComilla[0]=='"'){
											for(int cont=2;cont<verificarComilla.length;cont++){
												if(verificarComilla[cont]=='"'){
													String verificarComilla2[]=comandos[4].split("\"");
													accion="show history buy downline";
													objeto.put("down",verificarComilla2[1]);
												}
											}
										}else{
											accion="sintaxis";											
										}
										if(comandos.length>=6){
											accion="";
										}
									}else{
										accion="sintaxis";										
									}
								}else{
									accion="sintaxis";									
								}
							}	
						}else{
							if(comandos[2].equalsIgnoreCase("downline")){
								if(comandos.length==4){
										char[] verificarComilla=comandos[3].toCharArray();
										if(verificarComilla[0]=='"'){
											for(int cont=2;cont<verificarComilla.length;cont++){
												if(verificarComilla[cont]=='"'){
													String verificarComilla2[]=comandos[3].split("\"");
													accion="show history downline";
													objeto.put("down",verificarComilla2[1]);
												}
											}
										}else{
											accion="sintaxis";											
										}
										if(comandos.length>=5){
											accion="";
										}
									}else{
										accion="sintaxis";										
									}								
							}
						}
					}
				}	
				if(accion.equalsIgnoreCase("show downline")){
					if(comandos.length>=3){
						char [] verificarComilla=comandos[2].toCharArray();
						if(verificarComilla[0]=='"'){
							if(verificarComilla[1]!='"'){
								for(int cont=2;cont<verificarComilla.length;cont++){
									if(verificarComilla[cont]=='"'){
										if(comandos.length==3){
											String verificarComillas[]=comandos[2].split("\"");
											objeto.put("down",verificarComillas[1]);
										}else{
											if(comandos.length>=4){
												if(comandos.length==4){
													if(comandos[3].equalsIgnoreCase("money")){
														accion="show downline money";
														String verificarComillas[]=comandos[2].split("\"");
														objeto.put("down",verificarComillas[1]);													
													}else{
														accion="sintaxis";														
													}
												}else{
													if(comandos.length==5){
														if(comandos.length==5){
															if(comandos[4].equalsIgnoreCase("left")||comandos[4].equalsIgnoreCase("right")){
															accion="show downline money "+comandos[4];
															String verificarComillas[]=comandos[2].split("\"");
															objeto.put("down",verificarComillas[1]);				
															objeto.put("pierna",comandos[4].toLowerCase());
															}else{
																accion="sintaxis";																
															}
														}else{
															accion="sintaxis";															
														}
													}
												}
											}
										}
									}else{
										accion="sintaxis";
									}
								}	
							}else{
								accion="sintaxis";
							}
						}else{
							accion="sintaxis";
						}
					}else{
						accion="sintaxis";
					}
				}
				if(accion.equals("remove product")){
					if(comandos.length==3){
						char[]charArray=comandos[2].toCharArray();
						if(charArray[0]=='"'){
							for(int cont=2;cont<charArray.length;cont++){
								if(charArray[cont]=='"'){
									String verficarComas[]=comandos[2].split("\"");
									objeto.put("producto",verficarComas[1]);
								}
							}
						}else{
							accion="sintaxis";
						}
					}else{
						if(comandos.length==4){
							String valor=comandos[2]+comandos[3];
							char[]charArray=valor.toCharArray();
							if(charArray[0]=='"'){
								for(int cont=2;cont<charArray.length;cont++){
									if(charArray[cont]=='"'){
										String verficarComas[]=comandos[2].split("\"");
										objeto.put("producto",verficarComas[1]);
									}
								}
							}else{
								accion="sintaxis";								
							}
						}else{
							accion="sintaxis";
						}
					}
				}
				if(accion.equalsIgnoreCase("show me")){
					if(comandos.length>=3){
							if(comandos[2].equals("money")){
								if(comandos.length==3){
									accion=comandos[0]+" "+comandos[1]+" "+comandos[2];
								}else{
									accion="";
								}
							}else{
								accion="";
								
							}
						if(comandos.length==4){
							if(comandos[3].equals("right")||comandos[3].equals("left")){
								accion=comandos[0]+" "+comandos[1]+" "+comandos[2]+" "+comandos[3];
							}else{
								accion="";
							}
						}
					}
				}
			}
		
		return accion;
	}
}							
