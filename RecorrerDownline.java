package org.jarriaza.utilidad;
import org.jarriaza.beans.Downline;
import org.jarriaza.manejadores.ManjeadorDownline;
import org.jarriaza.manejadores.ManjeadorUsuario;
/**
	Clase que verifica downlines
*/
public class RecorrerDownline{
	/**
		Metodo en el que se verifica si el donwline esta arriba o abajo si esta arriba deuvlve false y si esta abajo true
		@param Downline downline
		@param int idDown
		@return boolean true or false
	*/
	public static boolean recorrerDown(Downline downline, int idDown){
		int idDownline=ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()).getIdDownline();
		if(idDownline==1){
				return true;
		}else{
			if(idDownline!=idDown){
				if(downline.getIdDownlineUp()!=0){					
					if(downline.getIdDownlineUp()==idDownline){
						System.out.println("entro aqui");
						return true;
					}else{
						System.out.println("entro aqui3");
						Downline down=ManjeadorDownline.getInstanciar().buscarDownline(downline.getIdDownlineUp());
						recorrerDown(down,idDown);
					}
				}
			}else{
				return true;
			}
		}
		return false;
	}
}