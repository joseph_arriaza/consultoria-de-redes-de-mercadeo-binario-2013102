package org.jarriaza.sistema;
/**
	Esta es la clase principal del programa
*/
public class App{
	/**
		Metodo principal que manda a llamar a Principal.
	*/
	public static void main(String args[]){
		new Principal().iniciar();
	}
}