package org.jarriaza.utilidad;
import org.jarriaza.beans.Usuario;
import org.jarriaza.manejadores.ManjeadorUsuario;
import org.jarriaza.manejadores.ManjeadorProducto;
import org.jarriaza.manejadores.ManjeadorDownline;
/**
	Clase en donde se edita nombre o producto
*/
public class EditMe {	
	/**
		Edita un usuario depende la propiedad a editar
		@param String prodpiedad propiedad a editra
		@param String valorA es el valor antiguo y se verifica que sea correcto
		@param String valorN es el nuevo valor
	*/
	public static void editar(String propiedad,String valorA,String valorN){
		int posicion=ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()-1;
		int poss=ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()).getIdDownline()-1;
		if(propiedad.equalsIgnoreCase("nickname")){
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getNickname().equals(valorA)){
				if(ManjeadorUsuario.getInstanciar().buscarUsuario(valorN)==null){
					System.out.println("	Su nickname es  : "+valorA+"			su nuevo nickname  sera: "+valorN);	
					ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(posicion).setNickname(valorN);
					ManjeadorDownline.getInstanciar().obtenerLista().get(poss).setNickname(valorN);
				}else{
					System.out.println("	El nickname ya existe.");
				}
			}else{
				System.out.println("	Su nickname no es correcto.");
			}
		}
		if(propiedad.equalsIgnoreCase("pass")){
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getPass().equals(valorA)){
				System.out.println("	Su password es  : "+valorA+"			su nuevo password  sera: "+valorN);														
				ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(posicion).setPass(valorN);
			}else{
				System.out.println("	Su password no es correcta.");
			}
		}
		if(propiedad.equalsIgnoreCase("nombre")){
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getNombre().equals(valorA)){
				System.out.println("	Su nombre es	: "+valorA+"		su nuevo nombre    sera: "+valorN);
				ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(posicion).setNombre(valorN);
				ManjeadorDownline.getInstanciar().obtenerLista().get(poss).setNombre(valorN);
			}else{
				System.out.println("	Su nombre   no es correcto.");
			}
		}
		if(propiedad.equalsIgnoreCase("apellido")){
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getApellido().equals(valorA)){
				System.out.println("	Su apellido es	: "+valorA+"		su nuevo apellido  sera: "+valorN);
				ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(posicion).setApellido(valorN);
			}else{
				System.out.println("	Su apellido no es correcto.");
			}
		}
		if(propiedad.equalsIgnoreCase("edad")){
			boolean verificar=ComandosProductos.isNumeric(valorA);
			if(verificar==true){
				int edadA=Integer.parseInt(valorA);
				boolean verificar2=ComandosProductos.isNumeric(valorN);
				if(verificar2==true){
					int edadN=Integer.parseInt(valorN);
					if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getEdad()==edadA){
						System.out.println("	Su edad es      : "+edadA+"		su nueva edad      sera: "+edadN);
						ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(posicion).setEdad(edadN);
					}else{
						System.out.println("	Su edad no es correcta.");
					}
				}else{
					System.out.println("	La edad debe de ser de tipo numerico.");
				}
			}else{
				System.out.println("	La edad debe de ser de tipo numerico.");
			}
		}
		if(propiedad.equalsIgnoreCase("direccion")){
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getDireccion().equals(valorA)){
				System.out.println("	Su direccion es : "+valorA+"	su nueva direccion sera: "+valorN);
				ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(posicion).setDireccion(valorN);
			}else{
				System.out.println("	Su direccion no es correcta.");
			}
		}
		if(propiedad.equalsIgnoreCase("email")){
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getEmail().equals(valorA)){
				System.out.println("	Su email  es	: "+valorA+"	su nuevo email     sera:	"+valorN);
				ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(posicion).setEmail(valorN);
			}else{
				System.out.println("	Su email no es correcto.");
			}
		}
	}
	/**
		Edita un producto depende la propiedad a editar
		@param String prodpiedad propiedad a editra
		@param String valorA es el valor antiguo y se verifica que sea correcto
		@param String valorN es el nuevo valor
	*/
	public static String editarProducto(String propiedad,String valorA,String valorN,String productoName){
		if(propiedad.equals("producto")){
			boolean verificar=ManjeadorProducto.getInstancia().verificarProducto(valorA);
			if(verificar==true){
				int posicion=ManjeadorProducto.getInstancia().buscarProducto(valorA).getIdProducto()-1;
				ManjeadorProducto.getInstancia().mostrarProducto().get(posicion).setProducto(valorN);
				productoName=valorN;
				System.out.println("	Nombre del Producto editado correctamente.");
				return productoName;
			}else{
				System.out.println("	El nombre del producto es incorrecto.");
			}
		}
		if(propiedad.equals("cantidad")){
			if(productoName!=null){
				if(ComandosProductos.isNumeric(valorA)==true){
					int cantidadA=Integer.parseInt(valorA);
					if(cantidadA==ManjeadorProducto.getInstancia().buscarProducto(productoName).getCantidad()){
						if(ComandosProductos.isNumeric(valorN)==true){						
							int cantidadN=Integer.parseInt(valorN);
							boolean verificar=ManjeadorProducto.getInstancia().verificarProducto(productoName);
							if(verificar==true){
								int posicion=ManjeadorProducto.getInstancia().buscarProducto(productoName).getIdProducto()-1;
								ManjeadorProducto.getInstancia().mostrarProducto().get(posicion).setCantidad(cantidadN);
								System.out.println("	Cantidad del Producto editada correctamente.");
								return productoName;
							}else{
								System.out.println("	El nombre del producto es incorrecto.");
							}
						}else{
							System.out.println("	La cantidad debe de ser tipo numerico.");					
						}
					}else{
						System.out.println("	La cantidad es incorrecta.");
					}
				}else{
					System.out.println("	La cantidad debe de ser tipo numerico.");	
				}
			}
		}
		if(propiedad.equals("precio")){
			if(productoName!=null){
				if(ComandosProductos.isNumeric(valorA)==true){
					int precioA=Integer.parseInt(valorA);
					if(precioA==ManjeadorProducto.getInstancia().buscarProducto(productoName).getPrecio()){
						if(ComandosProductos.isNumeric(valorN)==true){						
							int precioN=Integer.parseInt(valorN);
							boolean verificar=ManjeadorProducto.getInstancia().verificarProducto(productoName);
							if(verificar==true){
								int posicion=ManjeadorProducto.getInstancia().buscarProducto(productoName).getIdProducto()-1;
								ManjeadorProducto.getInstancia().mostrarProducto().get(posicion).setPrecio(precioN);
								System.out.println("	Precio del Producto editada correctamente.");
								return productoName;
							}
						}else{
							System.out.println("	El nombre del producto es incorrecto.");
						}
					}else{
						System.out.println("	El precio es incorrecto.");					
					}
				}else{
					System.out.println("	El precio debe de ser tipo numerico.");
				}
			}else{
				System.out.println("	El precio debe de ser tipo numerico.");	
			}
		}
		return productoName;
	}
}
