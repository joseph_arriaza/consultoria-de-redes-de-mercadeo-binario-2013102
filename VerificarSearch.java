package org.jarriaza.utilidad;
import org.jarriaza.manejadores.ManjeadorDownline;
import org.jarriaza.manejadores.ManjeadorUsuario;
import java.util.HashMap;
/**
	Clase en la que se resume el tipo de busqueda a realizar
*/
public class VerificarSearch{
	/**
		Imprime los valores que se envien de los usuarios
		@param String valores
	*/
	public static void imprimirValores(String valores){
		String [] usuarios=valores.split(" ");
		System.out.println("__________________________________________________");
		for(String recorrer:usuarios){
			System.out.println("	Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(recorrer)).getNombre());
			System.out.println("	Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(recorrer)).getConsumo());
			System.out.println("__________________________________________________");
		}
	}
	/**
		Quita las comillas de los comandos no decodificados
		@param String variable
		@param HashMap<String,String> objeto
	*/
	public static void quitarComilla(String variable, HashMap<String,String> objeto){
		if(objeto.get(variable)!=null){
			char [] verificandoChar=objeto.get(variable).toCharArray();
			if(verificandoChar[0]=='"'){
				if(verificandoChar[verificandoChar.length-1]=='"'){								
					String quitarComilla[]=objeto.get(variable).split("\"");
					if(quitarComilla.length==2){
						objeto.put(variable,quitarComilla[1]);
					}else{
						System.out.println("	Error de sintaxis.");
					}
				}else{
					System.out.println("	Error de sintaxis.");
				}
			}else{
				System.out.println("	Error de sintaxis.");
			}
		}
	}
	/**
		Verifica el tipo de busqueda, de que campo
		@param HashMap<String,String> objeto
	*/
	public static void verificar(HashMap<String,String> objeto){		
		if(objeto.get("nombre")!=null && objeto.get("email")!=null && objeto.get("edad")!=null){
			if(ComandosProductos.isNumeric(objeto.get("edad"))==true){
				quitarComilla("nombre",objeto);
				quitarComilla("edad",objeto);
				quitarComilla("email",objeto);
				if(SearchDownline.search(objeto.get("nombre"),objeto.get("email"),objeto.get("edad"))!=null){
					String valores=SearchDownline.search(objeto.get("nombre"),objeto.get("email"),objeto.get("edad"));
					imprimirValores(valores);
				}else{
					System.out.println("	No se encontro a nadie con estas coincidencias.");
				}	
			}else{
				System.out.println("	Para buscar por edad debes ingresar un tipo numerico.");
			}
		}else{
			if(objeto.get("nombre")!=null && objeto.get("email")!=null){
				quitarComilla("nombre",objeto);
				quitarComilla("email",objeto);
				if(SearchDownline.searchNombreEmail(objeto.get("nombre"),objeto.get("email"))!=null){
					String valores=SearchDownline.searchNombreEmail(objeto.get("nombre"),objeto.get("email"));
					imprimirValores(valores);
				}else{
					System.out.println("	No se encontro a nadie con estas coincidencias.");
				}
			}else{
				if(objeto.get("nombre")!=null && objeto.get("edad")!=null){
					quitarComilla("nombre",objeto);
					quitarComilla("edad",objeto);
					if(ComandosProductos.isNumeric(objeto.get("edad"))){
						if(SearchDownline.searchNombreEdad(objeto.get("nombre"),objeto.get("edad"))!=null){
							String valores=SearchDownline.searchNombreEdad(objeto.get("nombre"),objeto.get("edad"));
							imprimirValores(valores);
						}else{
							System.out.println("	No se encontro a nadie con estas coincidencias.");
						}
					}else{
						System.out.println("	Para buscar por edad debes ingresar un tipo numerico.");
					}
				}else{
					if(objeto.get("edad")!=null && objeto.get("email")!=null){
						quitarComilla("email",objeto);
						quitarComilla("edad",objeto);
						if(ComandosProductos.isNumeric(objeto.get("edad"))){
							if(SearchDownline.searchEdadEmail(objeto.get("edad"),objeto.get("email"))!=null){
								String valores=SearchDownline.searchEdadEmail(objeto.get("edad"),objeto.get("email"));
								imprimirValores(valores);
							}else{
								System.out.println("	No se encontro a nadie con estas coincidencias.");
							}
						}else{
							System.out.println("	Para buscar por edad debes ingresar un tipo numerico.");
						}
					}else{
						if(objeto.get("nombre")!=null){
							quitarComilla("nombre",objeto);
							if(SearchDownline.searchNombre(objeto.get("nombre"))!=null){
								String valores=SearchDownline.searchNombre(objeto.get("nombre"));
								String [] usuarios=valores.split(" ");
								imprimirValores(valores);
							}else{
								System.out.println("	No se encontro a nadie con estas coincidencias.");
							}
						}	
						if(objeto.get("edad")!=null){
							quitarComilla("edad",objeto);
							if(ComandosProductos.isNumeric(objeto.get("edad"))==true){
								if(SearchDownline.searchEdad(objeto.get("edad"))!=null){
									String valores=SearchDownline.searchEdad(objeto.get("edad"));
									imprimirValores(valores);
								}else{
									System.out.println("	No se encontro a nadie con estas coincidencias.");
								}
							}else{
								System.out.println("	Para buscar por edad debes ingresar un tipo numerico.");
							}
						}
						if(objeto.get("email")!=null){
							quitarComilla("email",objeto);
							if(SearchDownline.searchEmail(objeto.get("email"))!=null){
								String valores=SearchDownline.searchEmail(objeto.get("email"));
								imprimirValores(valores);
							}else{
								System.out.println("	No se encontro a nadie con estas coincidencias.");
							}
						}
					}
				}
			}
		}
	}
}