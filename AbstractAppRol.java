package org.jarriaza.app;
import java.util.HashMap;
import java.util.Stack;
import org.jarriaza.beans.Producto;
import org.jarriaza.utilidad.LeerDato;
import org.jarriaza.utilidad.Decodificador;
import org.jarriaza.utilidad.ShowMe;
import org.jarriaza.utilidad.ComandosProductos;
import org.jarriaza.utilidad.SearchDownline;
import org.jarriaza.utilidad.VerificarSearch;
import org.jarriaza.utilidad.RecorrerDownline;
import org.jarriaza.utilidad.Calc;
import org.jarriaza.beans.Downline;
import org.jarriaza.beans.Compra;
import org.jarriaza.beans.Oferta;
import org.jarriaza.manejadores.ManjeadorUsuario;
import org.jarriaza.manejadores.ManjeadorDownline;
import org.jarriaza.manejadores.ManjeadorProducto;
import org.jarriaza.manejadores.ManjeadorCompra;
import org.jarriaza.manejadores.ManjeadorOferta;
/**
	Clase abstracta en donde ambos roles puede usar las funciones.
	@param Decodificador Requiere un decodificador de tipo Decodificador
	@param boolean Connected con este se indica si la conexion esta actica o no
*/
public abstract class AbstractAppRol{
	private Decodificador decode;
	private boolean connected;
	/**
		Con esto se indica que la conexion esta activa
	*/
	public AbstractAppRol(){
		this.setConnected(true);
	}	
	/**
		Con este metodo se escoje que funcion elegira, o que rol es
	*/
	public void setDecodificador(Decodificador decodificador){
		this.decode=decodificador;
	}
	/**
		Este metodo mira si esta conectado
		@return boolean Retorna si el usuario esta conectado
	*/
	public boolean isConnected(){
		return true;
	}
	/**
		Este metodo sirve para aceptar una conexion o decir que ya se conecto
		@param boolean connected
	*/
	public void setConnected(boolean connected){
		this.connected=connected;
	}
	/**
		Con este metodo se ve el dinero de ambas piernas
		@param HashMap<String,String> objeto recibe un HashMap para escribir en el objeto los datos y no estar retornando variables
	*/
	public void verMoney(HashMap<String,String> objeto){
		AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))),0,objeto);
		AppMiembro.mostrarPiernaLeft(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))),0,objeto);
		if(objeto.get("derecha")==null){
			objeto.put("derecha","0");
		}
		if(objeto.get("izquierda")==null){
			objeto.put("izquierda","0");
		}
		if(Integer.parseInt(objeto.get("izquierda"))<=Integer.parseInt(objeto.get("derecha"))){
			System.out.println("	El volumen de su pierna izquierda es de :	"+objeto.get("izquierda"));
			System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("izquierda"))/10);
		}
		if(Integer.parseInt(objeto.get("izquierda"))>Integer.parseInt(objeto.get("derecha"))){
			System.out.println("	El volumen de su pierna derecha es de   :	"+objeto.get("derecha"));
			System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("derecha"))/10);
		}
	}
	/**
		Imprime la historia de los downlines agregados respecto a un downline especifico o uno mismo
		@param int idDownline lo usa pare reconocer de quien obtener la historia.
	*/
	public static void imprimirHistory(int idDownline){
		if(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getPersonasAgregadas()!=null){
			String historialAgregados[]=ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getPersonasAgregadas().split(" ");
				for(String recorrer:historialAgregados){
					int idUser=Integer.parseInt(recorrer);					
					int poss=idUser-1;
					System.out.println("____________________________________________");
					System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().obtenerLista().get(poss).getNombre());
					System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().obtenerLista().get(poss).getConsumo());
				}
					System.out.println("____________________________________________");
		}else{
			System.out.println("	Este downline no ha agregado a nadie.");
		}
	}
	/**
		Imprime las compras de un usuario especifico o de uno mismo si no se especifica.
		@param Sting nickDownline Requiere un nickname para ver de quien se buscaran las compras
	*/
	public static void imprimiCompra(String nickDownline){
		String compras[]=ManjeadorCompra.getInstancia().verMisCompras(nickDownline).split(" ");
		System.out.println("____________________________________________");
		for(String compra:compras){
			int idCompra=Integer.parseInt(compra);
			Compra impCompras=ManjeadorCompra.getInstancia().imprimiCompra(idCompra);
			System.out.println("Nickname      :		"+impCompras.getNickname());
			System.out.println("Producto      :		"+impCompras.getProducto());
			System.out.println("Fecha Pedido  :		"+impCompras.getFechaPedido());
			System.out.println("Fecha Entrega :		"+impCompras.getFechaEntrega());
			System.out.println("____________________________________________");
		}
	}
	/**
		Este es el metodo a donde se llega desde AppAdmin o AppMiembro si no se encuentra una funcion
		@param String accion requiere una accion para ver cual es el comando
		@param HashMap<String,String> objeto Requiere un HashMap para ingresar y mostrar datos
	*/
	public void avisarAccionar (String accion, HashMap<String, String> objeto){
		switch(accion){
		case "show me money right":	
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango().equals("miembro")){	
				AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()),0,objeto);
			}else{
				AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(1).getIdUsuario()),0,objeto);
			}
			if(objeto.get("derecha")==null){
				objeto.put("derecha","0");
			}
			System.out.println("	El volumen de su pierna derecha es de   :	"+objeto.get("derecha"));
			System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("derecha"))/10);
		break;
		case "show me money left":
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango().equals("miembro")){			
				AppMiembro.mostrarPiernaLeft(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()),0,objeto);
			}else{
				AppMiembro.mostrarPiernaLeft(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().buscarUsuario("user").getIdUsuario()),0,objeto);
			}
			if(objeto.get("izquierda")==null){
				objeto.put("izquierda","0");
			}
			System.out.println("	El volumen de su pierna izquierda es de :	"+objeto.get("izquierda"));
			System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("izquierda"))/10);
		break;
		case "show me money":		
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango().equals("miembro")){
				AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()),0,objeto);
				AppMiembro.mostrarPiernaLeft(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()),0,objeto);
				AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario()),0,objeto);
			}else{
				AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(1).getIdUsuario()),0,objeto);
				AppMiembro.mostrarPiernaLeft(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(1).getIdUsuario()),0,objeto);
				AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().obtenerListaUsuario().get(1).getIdUsuario()),0,objeto);
			}
			if(objeto.get("derecha")==null){
				objeto.put("derecha","0");
			}				
			if(objeto.get("izquierda")==null){
				objeto.put("izquierda","0");
			}
			if(Integer.parseInt(objeto.get("izquierda"))<=Integer.parseInt(objeto.get("derecha"))){
				System.out.println("	El volumen de su pierna izquierda es de :	"+objeto.get("izquierda"));
				System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("izquierda"))/10);
			}
			if(Integer.parseInt(objeto.get("izquierda"))>Integer.parseInt(objeto.get("derecha"))){
				System.out.println("	El volumen de su pierna derecha es de   :	"+objeto.get("derecha"));
				System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("derecha"))/10);
			}
		break;
		case "list products":
			ComandosProductos.mostrarProductos();
			break;
		case "show me":
			ShowMe.mostrarMisEspecificaciones(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango(),objeto);
			break;
		case "sintaxis":		
			System.out.println("	Erorr de sintaxis");	
		break;
		case "show product":
			if(objeto.get("product")!=null){
				if(ManjeadorProducto.getInstancia().verificarProducto(objeto.get("product"))==true){
					System.out.println("__________________________________________");
					Producto producto=ManjeadorProducto.getInstancia().buscarProducto(objeto.get("product"));
					System.out.println("Producto:	"+producto.getProducto());
					System.out.println("Precio:		"+producto.getPrecio());
					System.out.println("__________________________________________");
				}else{
					System.out.println("	El producto no existe.");
				}
			}else{
				System.out.println("	La sintaxis del comando es show product \"producto\"");
			}
		break;
		case "help":
		System.out.println();
		System.out.println("	list products");
		System.out.println("		list products");
		System.out.println();
		System.out.println("	show product ");
		System.out.println("		show product nombre del producto");
		System.out.println();
		System.out.println("	show downline");
		System.out.println("		show downline idDownline [money [left|right]]");		
		System.out.println();
		System.out.println("	show me");
		System.out.println("		show me [money |left|right|]");
		System.out.println();
		System.out.println("	show history");
		System.out.println("		show history [buy| |downline| [idDownline]");
		System.out.println();
		System.out.println("	search downline");
		System.out.println("		search downline datos**");
		System.out.println();
		System.out.println("	logout");
		System.out.println("		logout");
		System.out.println();
			if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango().equals("miembro")){
				System.out.println("	add downline");
				System.out.println("		add downline datos* [down| [idDownline] producto |left|right]");
				System.out.println("");
				System.out.println("	buy product");
				System.out.println("		buy product nombre del producto [num. tarjeta]");
				System.out.println("");
				System.out.println("	edit me");
				System.out.println("		edit me propiedad=valorAntiguo,nuevoValor");		
				System.out.println();
				System.out.println("	list downlines");
				System.out.println("		list downlines [idDownline| [left|right]");		
			}else{
				System.out.println("");
				System.out.println("	[add|remove|edit] product");
				System.out.println("		[add|remove|edit] product datos del producto");
				System.out.println("");
				System.out.println("	show sales");
				System.out.println("		show sales [product producto]");
				System.out.println("");
				System.out.println("	add user");
				System.out.println("		add user datos*");
			}
		break;
		case "search downline":
			VerificarSearch.verificar(objeto);
		break;
		case "show history downline":
			if(objeto.get("down")!=null){
				if(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down")))!=null){
					if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){
						int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
						if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
							AbstractAppRol.imprimirHistory(Integer.parseInt(objeto.get("down")));
						}else{
								System.out.println("	No puedes ver la historia de este downline.");
						}					
					}else{
						AbstractAppRol.imprimirHistory(Integer.parseInt(objeto.get("down")));
					}
				}else{
					System.out.println("	El downline no existe.");
				}
			}
		break;
		case "show downline money left":
			if(objeto.get("down")!=null){
				if(ComandosProductos.isNumeric(objeto.get("down"))==true){
					if(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down")))!=null){
						if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
								AppMiembro.mostrarPiernaLeft(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))),0,objeto);
								if(objeto.get("izquierda")==null){
									objeto.put("izquierda","0");
								}
								System.out.println("	El volumen de su pierna izquierda es de :	"+objeto.get("izquierda"));
								System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("izquierda"))/10);
							}else{
								System.out.println("	No puedes ver la historia money de este downline.");
							}
						}else{
							AppMiembro.mostrarPiernaLeft(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))),0,objeto);
							if(objeto.get("izquierda")==null){
								objeto.put("izquierda","0");
							}
							System.out.println("	El volumen de su pierna izquierda es de :	"+objeto.get("izquierda"));
							System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("izquierda"))/10);
						}
					}else{
						System.out.println("	El downline no existe.");
					}
				}else{
					System.out.println("	El idDownline debe de ser tipo numerico.");
				}
			}
		break;
		case "list downlines down right":
			if(objeto.get("down")!=null && objeto.get("pierna")!=null){				
				if(ComandosProductos.isNumeric(objeto.get("down"))==true){
					if(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down")))!=null){
						if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							//if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
								if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
									if(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()!=null){
										System.out.println("	Downlines lado izquierdo");
										System.out.println("_____________________________________________________________________");
										System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getNombre());
										System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getConsumo());
									}else{
										System.out.println("	El usuario no tiene downlines del lado izquierdo.");
									}
								}else{
									System.out.println("	No puedes ver left downline de este downline.");
								}
							//}else{
							//	System.out.println("	No puedes ver left downline de este downline.");
							//}
						}else{
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							if(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()!=null){
								System.out.println("	Downlines lado izquierdo");
								System.out.println("_____________________________________________________________________");
								System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getNombre());
								System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getConsumo());
							}else{
								System.out.println("	El usuario no tiene downlines del lado izquierdo.");
							}
						}
					}else{
						System.out.println("	Este downline no existe.");
					}
				}else{
					System.out.println("	El idDownline debe de ser numerico.");
				}
			}
		break;
		case "list downlines down left":
			if(objeto.get("down")!=null && objeto.get("pierna")!=null){				
				if(ComandosProductos.isNumeric(objeto.get("down"))==true){
					if(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down")))!=null){
						if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							//if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){								
								if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
									if(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()!=null){
										System.out.println("	Downlines lado izquierdo");
										System.out.println("_____________________________________________________________________");
										System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getNombre());
										System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getConsumo());
									}else{
										System.out.println("	El usuario no tiene downlines del lado izquierdo.");
									}
								}else{
									System.out.println("	No puedes ver left downline de este downline.");
								}
							/*}else{
								System.out.println("	El downline solo puede ser un usuario.");
							}*/
						}else{
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							if(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()!=null){
								System.out.println("	Downlines lado izquierdo");
								System.out.println("_____________________________________________________________________");
								System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getNombre());
								System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getConsumo());
							}else{
								System.out.println("	El usuario no tiene downlines del lado izquierdo.");
							}
						}
					}else{
						System.out.println("	Este downline no existe.");
					}
				}else{
					System.out.println("	El idDownline debe de ser numerico.");
				}
			}						
		break;
		case "list downlines down":
			if(objeto.get("down")!=null){
				if(ComandosProductos.isNumeric(objeto.get("down"))==true){
					if(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down")))!=null){
						if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();									
								if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
									if(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()!=null){
										System.out.println("	Downlines lado izquierdo");
										System.out.println("_____________________________________________________________________");
										System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getNombre());
										System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getConsumo());
									}else{
										System.out.println("	El usuario no tiene downlines del lado izquierdo.");
									}
									System.out.println("_____________________________________________________________________");
									if(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getRight()!=null){
										System.out.println("	Downlines lado derecho");
										System.out.println("_____________________________________________________________________");
										System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getRight()).getNombre());
										System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getRight()).getConsumo());
									}else{
										System.out.println("	El usuario no tiene downlines del lado derecho.");
									}
									System.out.println("_____________________________________________________________________");
								}else{
									System.out.println("	No puedes ver los downlines de este downline.");
								}
						}else{
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							if(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()!=null){
								System.out.println("	Downlines lado izquierdo");
								System.out.println("_____________________________________________________________________");
								System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getNombre());
								System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getLeft()).getConsumo());
							}else{
								System.out.println("	El usuario no tiene downlines del lado izquierdo.");
							}
							System.out.println("_____________________________________________________________________");
							if(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getRight()!=null){
								System.out.println("	Downlines lado derecho");
								System.out.println("_____________________________________________________________________");
								System.out.println("Nombre		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getRight()).getNombre());
								System.out.println("Consumo		:		"+ManjeadorDownline.getInstanciar().buscarDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline).getRight()).getConsumo());
							}else{
								System.out.println("	El usuario no tiene downlines del lado derecho.");
							}
							System.out.println("_____________________________________________________________________");
						}
					}else{
						System.out.println("	Este downline no existe.");
					}
				}else{
					System.out.println("	El idDownline debe de ser numerico.");
				}
			}
		break;
		case "show downline money right":
			if(objeto.get("down")!=null){
				if(ComandosProductos.isNumeric(objeto.get("down"))==true){
					if(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down")))!=null){
						if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){
						int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
								AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))),0,objeto);
								if(objeto.get("derecha")==null){
									objeto.put("derecha","0");
								}
								System.out.println("	El volumen de su pierna derecha es de   :	"+objeto.get("derecha"));
								System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("derecha"))/10);
							}else{
								System.out.println("	No puedes ver la historia money de este downline.");
							}
						}else{
							AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))),0,objeto);
							if(objeto.get("derecha")==null){
									objeto.put("derecha","0");
							}
							System.out.println("	El volumen de su pierna derecha es de   :	"+objeto.get("derecha"));
							System.out.println("	Por lo tanto su ganancia seria de       :	"+Integer.parseInt(objeto.get("derecha"))/10);
						}
					}else{
						System.out.println("	El downline no existe.");
					}
				}else{
					System.out.println("	El idDownline debe de ser tipo numerico.");
				}
			}
		break;
		case "show downline money":
			if(objeto.get("down")!=null){
				if(ComandosProductos.isNumeric(objeto.get("down"))==true){
					if(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down")))!=null){
						if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
								this.verMoney(objeto);
							}else{
								System.out.println("	No puedes ver la historia money de este downline.");
							}
						}else{
							this.verMoney(objeto);
						}
					}else{
						System.out.println("	El downline no existe.");
					}
				}else{
					System.out.println("	El idDownline debe de ser tipo numerico.");
				}
			}
		break;
		case "show downline":
			if(objeto.get("down")!=null){
				if(ComandosProductos.isNumeric(objeto.get("down"))==true){
					if(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down")))!=null){
						if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
								String nick=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getNickname();
								ShowMe.mostrarEspecificacionesDownline(nick,objeto);
							}else{
								System.out.println("	No puedes ver la informacion de este downline.");
							}
						}else{
							int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
							String nick=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getNickname();
							ShowMe.mostrarEspecificacionesDownline(nick,objeto);
						}
					}else{
						System.out.println("	El downline no existe.");
					}
				}else{
					System.out.println("	El idDownline debe de ser numerico.");
				}
			}else{
				System.out.println("	La sintaxis del comando es show downline \"idDownline\"");
			}
		break;
		case "show history buy downline":
				if(objeto.get("down")!=null){
					if(ComandosProductos.isNumeric(objeto.get("down"))==true){
						if(ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down")))!=null){
							if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()=="miembro"){
								int idDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getIdDownline();
								if(RecorrerDownline.recorrerDown(ManjeadorDownline.getInstanciar().buscarDownline(idDownline),idDownline)==true){										
									String nick=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getNickname();
									if(ManjeadorCompra.getInstancia().verMisCompras(nick)!=null){
										AbstractAppRol.imprimiCompra(nick);
									}else{
										System.out.println("	Este downline no tiene compras.");
									}
								}else{
									System.out.println("	No puedes ver las compras de este downline.");
								}
							}else{
								String nickDownline=ManjeadorDownline.getInstanciar().buscarDownline(Integer.parseInt(objeto.get("down"))).getNickname();
								if(ManjeadorCompra.getInstancia().verMisCompras(nickDownline)!=null){
									AbstractAppRol.imprimiCompra(nickDownline);
								}else{
									System.out.println("	Este downline no tiene compras.");
								}
							}
						}else{
							System.out.println("	El downline no existe.");
						}
					}else{
						System.out.println("	El idDownline debe de ser numerico.");
					}
				}else{
					System.out.println("	Debes ingresar un downline a buscar.");
				}
		break;
		case "list offert":
			if(ManjeadorOferta.getInstancia().mostrarOfertas().size()>=1){
				System.out.println("_________________________________________________");
				for(Oferta oferta:ManjeadorOferta.getInstancia().mostrarOfertas()){
					System.out.println("	Producto        :   "+oferta.getProducto());
					if(oferta.getOferta().equals("2*1")){
						System.out.println("	Oferta          :   "+oferta.getOferta());
					}else{
						int descuento=Integer.parseInt(oferta.getOferta());
						descuento=100-descuento;
						System.out.println("	Oferta          :   "+descuento+"% de descuento");
					}
					System.out.println("	Fecha Oferta    :   "+oferta.getFechaOferta());
					System.out.println("_________________________________________________");
				}
			}else{
				System.out.println("	No existe ninguna oferta.");
			}
		break;
		case "calc":
			Stack<String> pila = new Stack<String>();
			String [] por=objeto.get("calc").split("\\*");
			int pos=0;
			String vale=null;
			int ultima=0;
			//System.out.println(por.length);
			String posicione=null;
			if(por.length>=2){
				char [] charArray=objeto.get("calc").toCharArray();
				for(int cont=0;cont<charArray.length;cont++){
					if(charArray[cont]=='*'||charArray[cont]=='+'){
						if(posicione==null){
							posicione=Integer.toString(cont);
							ultima=cont;
						}else{
							posicione=posicione+" "+cont;							
						}
					}
					ultima=cont;
				}
				String [] positions=posicione.split(" ");
				System.out.println(posicione);
				int finala=0;
				if(positions.length>=2){
					for(int cont=0;cont<positions.length;cont++){
						System.out.println(positions[cont]+" "+cont);
						if(cont==0){
							finala=Integer.parseInt(positions[cont]);
							String claveValor=objeto.get("calc").substring(0,finala);
							pila.push(claveValor);
						}else{
							finala=Integer.parseInt(positions[cont-1]);
							int principia=Integer.parseInt(positions[cont]);
							String claveValor=objeto.get("calc").substring(finala,principia);
							pila.push(claveValor);							
						}	
						if(cont==positions.length-1){							
							finala=Integer.parseInt(positions[cont]);
							String claveValor=objeto.get("calc").substring(finala,ultima+1);
							pila.push(claveValor);	
						}				
					}
				}
			}
			System.out.println("");
			while (!pila.empty())
  				System.out.println(pila.pop());
			//Calc.calcu(objeto);

		break;
		default:
			System.out.println("	Ingresa un comando valido.");
			System.out.println("	Presione help para obtener ayuda.");
			break;
		}
	}
	/**
		Este metodo inicia la ejecucion de comandos, con este se lee el comando
	*/
	public void iniciar(){
		String comando;
		do{
			System.out.print(ManjeadorUsuario.getInstanciar().usuarioLogeado().getNickname()+" >>");
			comando=LeerDato.getInstancia().leer();
			this.decode.decodificarComando(comando);
		}while(this.connected);
	}
}
