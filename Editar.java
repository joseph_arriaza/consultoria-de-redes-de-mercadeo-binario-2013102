package org.jarriaza.utilidad;
import org.jarriaza.manejadores.ManjeadorUsuario;
import org.jarriaza.manejadores.ManjeadorProducto;
import org.jarriaza.app.AppAdmin;
import java.util.HashMap;
/**
	Clase en la que se escoje editar un producto o un downline
*/
public class Editar{
	/**
		Con este metodo se verifica que se editara para mandar a EditMe y editar el dato
		@param String accion reconoce la accion que se va a editr o el campo
		@param HashMap<String,String> objeto
	*/
	public static void editarUsuario(String accion, HashMap<String, String> objeto){		
		if(objeto.get("nombre")!=null||objeto.get("apellido")!=null||objeto.get("nickname")!=null||objeto.get("pass")!=null||objeto.get("edad")!=null||objeto.get("direccion")!=null||objeto.get("email")!=null){
			if(objeto.get("nombre")!=null){
				String verificarComando[]=objeto.get("nombre").split(",");				
				if(verificarComando.length==2){
					EditMe.editar("nombre",verificarComando[0],verificarComando[1]);
				}else{
					System.out.println("	La sintaxis del comando es, edit me nombre=\"\"valorAntiguo,valorNuevo\"\"");
				}
			}
			if(objeto.get("apellido")!=null){
				String verificarComando[]=objeto.get("apellido").split(",");
				if(verificarComando.length==2){
					String barrerComilla[]=verificarComando[0].split("\"");
					String barrerComilla2[]=verificarComando[1].split("\"");
					EditMe.editar("apellido",barrerComilla[1],barrerComilla2[1]);
				}else{
					System.out.println("	La sintaxis del comando es, edit me apellido=\"valorAntiguo,valorNuevo\"");
				}
			}
			if(objeto.get("nickname")!=null){
				String verificarComando[]=objeto.get("nickname").split(",");
				if(verificarComando.length==2){
					EditMe.editar("nickname",verificarComando[0],verificarComando[1]);
				}else{
					System.out.println("	La sintaxis del comando es, edit me nickname=\"valorAntiguo,valorNuevo\"");
				}
			}
			if(objeto.get("pass")!=null){
				String verificarComando[]=objeto.get("pass").split(",");
				if(verificarComando.length==3){
					if(verificarComando[1].equals(verificarComando[2])){
						EditMe.editar("pass",verificarComando[0],verificarComando[1]);
					}else{
						System.out.println("	Compruebe su password.");
					}
				}else{
					System.out.println("	La sintaxis del comando es, edit me pass=\"valorAntiguo,valorNuevo\"");
				}
			}
			if(objeto.get("edad")!=null){
				String verificarComando[]=objeto.get("edad").split(",");
				if(verificarComando.length==2){
					EditMe.editar("edad",verificarComando[0],verificarComando[1]);					
				}else{
					System.out.println("	La sintaxis del comando es, edit me edad=\"valorAntiguo,valorNuevo\"");
				}
			}
			if(objeto.get("direccion")!=null){
				String verificarComando[]=objeto.get("direccion").split(",");
				if(verificarComando.length==2){
					EditMe.editar("direccion",verificarComando[0],verificarComando[1]);					
				}else{
					System.out.println("	La sintaxis del comando es, edit me direccion=\"valorAntiguo,valorNuevo\"");
				}
			}
			if(objeto.get("email")!=null){
				String verificarComando[]=objeto.get("email").split(",");
				if(verificarComando.length==2){
					if(AppAdmin.isEmail(verificarComando[1])){
						EditMe.editar("email",verificarComando[0],verificarComando[1]);	
					}else{
						System.out.println("	Su email debe de tener una @.");
					}
				}else{
					System.out.println("	La sintaxis del comando es, edit me email=\"valorAntiguo,valorNuevo\"");
				}
			}
		}else{
			System.out.println("	La sintaxis del comando es, edit me propiedad=\"valorAntiguo,valorNuevo\" .");
		}
	}
	/**
		Con este metodo se edita un producto
		@param String accion reconoce la accion que se va a editr o el campo
		@param HashMap<String,String> objeto
	*/
	public static void editarProducto(String accion, HashMap<String, String> objeto){
		String productName=null;
		String productCant=null;
		String productNameInt=null;
		if(objeto.get("producto")!=null||objeto.get("cantidad")!=null||objeto.get("precio")!=null){
			if(objeto.get("producto")!=null){				
				String verificarComando[]=objeto.get("producto").split(",");
				if(verificarComando.length==2){
					productName=EditMe.editarProducto("producto",verificarComando[0],verificarComando[1],null);
				}else{
					boolean verificar=ManjeadorProducto.getInstancia().verificarProducto(verificarComando[0]);
					if(verificar==true){
						if(objeto.get("cantidad")!=null||objeto.get("precio")!=null){
							if(objeto.get("cantidad")!=null){
								String verificarComandoCant[]=objeto.get("cantidad").split(",");
								if(verificarComandoCant.length==2){
									EditMe.editarProducto("cantidad",verificarComandoCant[0],verificarComandoCant[1],verificarComando[0]);
								}else{
									System.out.println("	La sintaxis del comando es edit producto producto=nombre cantidad=cantidadA,cantidadN.");
								}
							}
							if(objeto.get("precio")!=null){
								String verificarComandoPrec[]=objeto.get("precio").split(",");
								if(verificarComandoPrec.length==2){
									EditMe.editarProducto("precio",verificarComandoPrec[0],verificarComandoPrec[1],verificarComando[0]);
							
								}else{
									System.out.println("	La sintaxis del comando es edit producto producto=nombre precio=precioA,precioN.");
								}
							}
						}else{
							System.out.println("	La sintaxis del comando es, edit product producto=nombre propiedad=propiedadA,propiedadN");
						}
					}else{
						System.out.println("	El producto no existe.");
					}
					System.out.println("	Para editar el nombre de un producto la sintaxis es, edit producto producto=nombreA,nombreN.");
				}
			}
		}else{
			System.out.println("	La sintaxis del comando es propiedad=\"valorAntiguo,valorNuevo\".");
		}
		if(productName!=null){
			if(objeto.get("cantidad")!=null){
				String verificarProductoC[]=objeto.get("cantidad").split(",");
				if(verificarProductoC.length==2){
					EditMe.editarProducto("cantidad",verificarProductoC[0],verificarProductoC[1],productName);
					productCant=objeto.get("cantidad");
				}else{
					System.out.println("	La sintaxis del comando es, edit product producto=\"valorAntiguo,valorNuevo\" cantidad=cantidadAntigua,cantidadNueva");
				}					
			}
		}
		if(productName!=null){
			String verificarProducto[]=productName.split(",");
			if(objeto.get("precio")!=null){
				String verificarProductoP[]=objeto.get("precio").split(",");
				if(verificarProductoP.length==2){
					EditMe.editarProducto("precio",verificarProductoP[0],verificarProductoP[1],productName);
				}else{
					System.out.println("	La sintaxis del comando es, edit product producto=\"valorAntiguo,valorNuevo\" precio=precioAntiguo,precioNuevo");
				}	
			}
		}
	}
}
