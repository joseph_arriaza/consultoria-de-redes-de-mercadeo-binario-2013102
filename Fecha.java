package org.jarriaza.utilidad;
import java.util.Calendar;
import java.util.GregorianCalendar;
/**
	Metodo que da la fecha de compra y entrega
*/
public class Fecha{
	/**
		Metodo que reconoce la fecha del dia para la fecha del pedido
		@return String fecha del dia
	*/
	public static String fechaHoy(){
		Calendar fecha = new GregorianCalendar();
		int mes = fecha.get(Calendar.MONTH)+1;
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		int year = fecha.get(Calendar.YEAR);
		String fechaHoy=Integer.toString(dia)+"/"+"0"+Integer.toString(mes)+"/"+Integer.toString(year);
		return fechaHoy;
	}
	/**
		Metodo que reconoce la fecha del dia mas 7 dias para la fecha de entrega
		@return String fecha de entrega
	*/
	public static String fechaEntrega(){
		Calendar fecha = new GregorianCalendar();
		int mes = fecha.get(Calendar.MONTH)+1;
		int dia = fecha.get(Calendar.DAY_OF_MONTH)+7;
		if(dia>30){
			dia=dia-30;
			mes=mes+1;
		}
		int year = fecha.get(Calendar.YEAR);
		String fechaEntrega=Integer.toString(dia)+"/"+Integer.toString(mes)+"/"+Integer.toString(year);
		return fechaEntrega;
	}
}