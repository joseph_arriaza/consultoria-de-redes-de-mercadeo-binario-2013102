package org.jarriaza.utilidad;
/**
	Esta clase comprueba si la tarjeta de credito es valida.
*/
public class Tarjeta{
/**
	Este metodo verifica si la tarjeta tiene 16 numeros y si es valida o no, para esto se requiere que el usuario sea mayor de edad.
	@param tarjetaCred -Requiere un numero de tarjeta de credito.
	@return resultado -Devuelve si la tarjeta de credito es verdadera o si no lo es.
*/
	public static boolean esNumeroLargo(String numero){
		try{
			Long.parseLong(numero);
			return true;
		}catch(NumberFormatException nfe){
			return false;
		}
	}
	/**
		Este metodo comprueba si la tarjeta es valida, si su suma es divisible entre 10, etc.
		@param String tarjetaCred
	*/
	public static boolean comprobarTarjeta(String tarjetaCred){
		boolean resultado=false;
		int num[]=new int[17]; 
		int dato1, dato2;
		int result=0;
		String hola;
			if(tarjetaCred.length()==16){
				for(int contador=0;contador<=16;contador++){
					if(contador==0||contador==2||contador==4||contador==6||contador==8||contador==10||contador==12||contador==14){
						num[contador]=Integer.parseInt(tarjetaCred.substring(contador,contador+1));
						num[contador]=num[contador]*2;
						hola=Integer.toString(num[contador]);
						if(hola.length()==2){
							dato1=Integer.parseInt(hola.substring(0,1));
							dato2=Integer.parseInt(hola.substring(1,2));
							num[contador]=dato1+dato2;
						}
					}else{
						if(contador==1||contador==3||contador==5||contador==7||contador==9||contador==11||contador==13||contador==15){
							num[contador]=Integer.parseInt(tarjetaCred.substring(contador,contador+1));
							result+=num[contador];
							
						}
					}			
					
				}
			}
		result=num[0]+num[1]+num[2]+num[3]+num[4]+num[5]+num[6]+num[7]+num[8]+num[9]+num[10]+num[11]+num[12]+num[13]+num[14]+num[15];
		if(result%10==0){
			resultado=true;
		}
		return resultado;
	}
}