package org.jarriaza.beans;
/**
	Beans de Producto
*/
public class Producto {
	private int idProducto;
	private String producto;
	private int precio;
	private int cantidad;
	/**
		Retorna el id del producto
		@return int idProducto
	*/
	public int getIdProducto() {
		return idProducto;
	}
	/**
		Ingresa el id del producto
		@param int idProducto
	*/
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	/**
		Retorna el producto
		@return String produto
	*/
	public String getProducto() {
		return producto;
	}
	/**
		Ingresa el producto
		@param String producto
	*/
	public void setProducto(String producto) {
		this.producto = producto;
	}
	/**
		Retorna el precio del producto
		@return int precio
	*/
	public int getPrecio() {
		return precio;
	}
	/**
		Ingresa el precio del producto
		@param int cantidad
	*/
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	/**
		Retorna la cantidad del producto
		@return int cantidad
	*/
	public int getCantidad() {
		return cantidad;
	}
	/**
		Ingresa la cantidad del producto
		@param String cantidad
	*/
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	/**
		Constructor vacio
	*/
	public Producto() {
		super();
	}
	/**
		Constructor de compra
		@param int idProducto
		@param String producto
		@param int precio
		@param int cantidad
	*/
	public Producto(int idProducto,String producto, int precio, int cantidad) {
		this.idProducto=idProducto;
		this.producto = producto;
		this.precio = precio;
		this.cantidad = cantidad;
	}
}
