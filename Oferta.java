package org.jarriaza.beans;
public class Oferta{
	private String fechaOferta;
	private String producto;
	private String oferta;
	public void setFechaOferta(String fechaOferta){
		this.fechaOferta=fechaOferta;
	}
	public String getFechaOferta(){
		return this.fechaOferta;
	}
	public void setProducto(String producto){
		this.producto=producto;
	}
	public String getProducto(){
		return this.producto;
	}
	public void setOferta(String oferta){
		this.oferta=oferta;
	}
	public String getOferta(){
		return this.oferta;
	}
	public Oferta(){
		super();
	}
	public Oferta(String fecha,String product,String offert){
		this.fechaOferta=fecha;
		this.producto=product;
		this.oferta=offert;
	}
}