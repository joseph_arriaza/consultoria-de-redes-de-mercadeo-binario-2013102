package org.jarriaza.manejadores;
import java.util.ArrayList;
import org.jarriaza.beans.Usuario;
import java.util.Iterator;
import java.util.HashMap;
/**
	Manejador del beans Usuario
*/
public class ManjeadorUsuario {
	private static ManjeadorUsuario instanciar;
	private ArrayList<Usuario> listaUsuarios;
	private Usuario usuarioIniciado;
	/**
		Constructor privado que agrega 2 usuarios a la lista
	*/
	private ManjeadorUsuario(){
		listaUsuarios=new ArrayList<Usuario>();
		listaUsuarios.add(new Usuario(1,"admin","admin","Joseph","Arriaza","administrador",16,"jarriaza2013102@kinal.edu.gt","Ciudad"));
		listaUsuarios.add(new Usuario(2,"user","user","Juan","Perez","miembro",15,"algo@algo.com","Ciudad"));
	}
	/**
		Agrega un usuario
		@param Usuario usuario
	*/
	public void agregarUsuario(Usuario usuario){
		this.listaUsuarios.add(usuario);
	}
	/**
		Elimina un usuario
		@param Usuario usuario
	*/
	public void eliminarUsuario(Usuario usuario){
		this.listaUsuarios.remove(usuario);
	}
	/**
		Retorna la lista de usuarios
		@return ArrayList<Usuario> listaUsuarios
	*/
	public ArrayList<Usuario> obtenerListaUsuario(){
		return this.listaUsuarios;
	}
	/**
		Busca un usuario por nickname
		@param String nickname
		@return Usuario usuario
	*/
	public Usuario buscarUsuario(String nickname){
		for(Usuario usuario : listaUsuarios){
			if (usuario.getNickname().equals(nickname)){
				return usuario;
			}
		}
	
		return null;
	}
	/**
		Obtiene el ultimo id
		@return int lastId
	*/
	public int lastId(){
		int id=0;
		for(Usuario user:listaUsuarios){
			id=user.getIdUsuario();
		}
		return id;
	}
	/**
		Devulve true si el usuario y la password son correctos y si no false
		@param String nickanme
		@param String pass
		@return boolean true or false
	*/
	public boolean iniciarSesion(String nickname, String pass){
		Usuario usuario = buscarUsuario(nickname);
		if(usuario!=null){
			if(usuario.getPass().equals(pass)){
				this.usuarioIniciado=usuario;
				return true;
			}
		}
		return false;
	}
	/**
		Elimina el usuario de la sesion
	*/
	public void logoutUsuario(){
		this.usuarioIniciado=null;
	}
	/**
		Agrega un usuario a la sesion y lo retorna
		@return Usuario usuarioIniciado
	*/
	public Usuario usuarioLogeado(){
		return this.usuarioIniciado;
	}
	/**
		Metodo para instanciar una sola vez
		@return ManjeadorUsuario instanciar
	*/
	public static ManjeadorUsuario getInstanciar(){
		if(instanciar==null){
			instanciar=new ManjeadorUsuario();
		}
		return instanciar;
	}
	/**
		Verifica si existe un usuario por el nickname
		@param String nickname
		@return boolean true or false
	*/
	public boolean verificarUser(String nickname){
		for(Usuario usuario : listaUsuarios){
			if (usuario.getNickname().equals(nickname)){
				return true;
			}
		}
		return false;
	}
}
