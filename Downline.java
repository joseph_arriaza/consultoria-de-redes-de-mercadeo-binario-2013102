package org.jarriaza.beans;
import java.util.ArrayList;
/**
	beans de Downline
*/
public class Downline{
	private int idDownline;
	private int idUsuario;
	private String nickname;
	private String nombre;	
	private String personasAgregadas;
	private int idDownlineUp;
	private String left;
	private String right;
	private int consumo;
	private String producto;
	private boolean activo;
	private long tarjetaCred;
	private String persona;
	/**
		Se devuelve la persona que agrego
		@return String persona
	*/
	public String getPersona() {
		return persona;
	}
	/**
		Ingresa la persona que agrego
		@param String persona
	*/
	public void setPersona(String persona) {
		this.persona = persona;
	}
	/**
		Se devuelve el id
		@return int idUsuario
	*/
	public int getIdUsuario() {
		return idUsuario;
	}
	/**
		Ingresa el id
		@param int idUsuario
	*/
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	/**
		Ingresa la tarjeta
		@param long tarjeta
	*/
	public void setTarjetaCred(long tarjeta){
		this.tarjetaCred=tarjeta;
	}
	/**
		Se devuelve la tarjeta de credito
		@return String persona
	*/
	public long getTarjetaCred(){
		return tarjetaCred;
	}
	/**
		Ingresa si es activo true y si no false
		@param boolean activo
	*/
	public void setActivo(boolean activo){
		this.activo=activo;
	}
	/**
		Se devuelve true si es activo y false si no
		@return boolean activo
	*/
	public boolean getActivo(){
		return activo;
	}
	/**
		Ingresa el id del upline
		@param int id
	*/
	public void setIdDownlineUp(int id){
		this.idDownlineUp=id;
	}
	/**
		Se devuelve el id del upline
		@return int idDownlineUp
	*/
	public int getIdDownlineUp(){
		return idDownlineUp;
	}
	/**
		Ingresa el id del downline
		@param int id
	*/
	public void setIdDownline(int id){
		this.idDownline=id;
	}
	/**
		Se devuelve el id
		@return int idDownline
	*/
	public int getIdDownline(){
		return idDownline;
	}
	/**
		Se devuelve el nickname
		@return String nickname
	*/
	public String getNickname() {
		return nickname;
	}
	/**
		Ingresa el nickname
		@param String nickname
	*/
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
		Se devuelve el nombre
		@return String nombre
	*/
	public String getNombre() {
		return nombre;
	}
	/**
		Ingresa el nombre
		@param String nombre
	*/
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
		Ingresa los ids de las personas agregadas
		@param String personasAgregadas
	*/
	public void setPersonasAgregadas(String personasAgregadas) {
		this.personasAgregadas = personasAgregadas;
	}
	/**
		Se devuelve las peronsasAgregadas y sus ids
		@return String peronasAgregas
	*/
	public String getPersonasAgregadas() {
		return personasAgregadas;
	}
	/**
		Se devuelve el usuario del lado izquierdo
		@return String left
	*/
	public String getLeft() {
		return left;
	}
	/**
		Ingresa el nickname en la pierna izquierda
		@param String left
	*/
	public void setLeft(String left) {
		this.left = left;
	}
	/**
		Se devuelve el usuario del lado derecho
		@return String right
	*/
	public String getRight() {
		return right;
	}
	/**
		Ingresa el nickname en la pierna derecha
		@param String right
	*/
	public void setRight(String right) {
		this.right = right;
	}
	/**
		Se devuelve el consumo
		@return int consumo
	*/
	public int getConsumo() {
		return consumo;
	}
	/**
		Ingresa el consumo
		@param int consumo
	*/
	public void setConsumo(int consumo) {
		this.consumo = consumo;
	}
	/**
		Se devuelve el producto comprado
		@return String producto
	*/
	public String getProducto() {
		return producto;
	}
	/**
		Ingresa el producto comprado
		@param String producto
	*/
	public void setProducto(String producto) {
		this.producto = producto;
	}
	/**
		Constructor vacio
	*/
	public Downline(){
		super();
	}
	/**
		Constructor para downlines
		@param int id
		@param int idUser
		@param String nickname
		@param String nombre
		@param int idDownlineUp
		@param String producto
		@param int consumo
		@param boolean activo
		@param long tarjeta
		@param String persona
	*/
	public Downline(int id,int idUser,String nickname,String nombre,int idDownlineUp,String producto,int consumo,boolean activo, long tarjeta,String persona){
		setIdDownline(id);
		setIdUsuario(idUser);
		setNickname(nickname);
		setNombre(nombre);
		setIdDownlineUp(idDownlineUp);
		setProducto(producto);
		setConsumo(consumo);
		setActivo(activo);
		setTarjetaCred(tarjeta);
		setPersona(persona);
	}
}