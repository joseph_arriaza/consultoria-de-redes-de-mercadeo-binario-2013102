package org.jarriaza.manejadores;
import org.jarriaza.beans.Oferta;
import java.util.ArrayList;
public class ManjeadorOferta{
	private static ManjeadorOferta instancia;
	private ArrayList<Oferta> listaOfertas;
	private ManjeadorOferta(){
		listaOfertas=new ArrayList<Oferta>();
	}
	public void agregarOferta(Oferta oferta){
		listaOfertas.add(oferta);
	}
	public ArrayList<Oferta> mostrarOfertas(){
		return listaOfertas;
	}
	public void eliminarOferta(Oferta oferta){
		listaOfertas.remove(oferta);
	}
	public Oferta verOferta(String producto,String fecha){
		for(Oferta oferta:listaOfertas){
			if(oferta.getProducto().equals(producto)){
				if(oferta.getFechaOferta().equals(fecha)){
					return oferta;
				}
			}
		}
		return null;
	}
	public boolean verificarOferta(String producto,String fecha){		
		for(Oferta oferta:listaOfertas){
			if(oferta.getProducto().equals(producto)){
				if(oferta.getFechaOferta().equals(fecha)){
					return true;
				}
			}
		}
		return false;
	}
	public static ManjeadorOferta getInstancia(){
		if(instancia==null){
			instancia=new ManjeadorOferta();
		}
		return instancia;
	}
}