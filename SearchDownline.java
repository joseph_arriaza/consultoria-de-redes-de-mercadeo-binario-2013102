package org.jarriaza.utilidad;
import org.jarriaza.beans.Downline;
import org.jarriaza.manejadores.ManjeadorDownline;
import org.jarriaza.manejadores.ManjeadorUsuario;
/**
	Clase en la que se buscan los downlines
*/
public class SearchDownline{
	/**
		Metodo que busca por el nombre
		@param String nombre
		@return String idUser
	*/
	public static String searchNombre(String nombre){		
		String idUser=null;
		for(Downline downlineBuscar:ManjeadorDownline.getInstanciar().obtenerLista()){
			if(downlineBuscar.getNombre().contains(nombre)==true){
				if(idUser==null){
					idUser=Integer.toString(downlineBuscar.getIdDownline());
				}else{
					idUser=idUser+" "+downlineBuscar.getIdDownline();
				}
			}
		}		
		return idUser;
	}
	/**
		Metodo que busca por el nickname
		@param String nickname
		@return String idUser
	*/
	public static String searchNickname(String nickname){		
		String idUser=null;
		for(Downline downlineBuscar:ManjeadorDownline.getInstanciar().obtenerLista()){
			if(downlineBuscar.getNickname().contains(nickname)==true){
				if(idUser==null){
					idUser=Integer.toString(downlineBuscar.getIdDownline());
				}else{
					idUser=idUser+" "+downlineBuscar.getIdDownline();
				}
			}
		}		
		return idUser;
	}
	/**
		Metodo que busca por la edad
		@param String edad
		@return String idUser
	*/
	public static String searchEdad(String edad){		
		String idUser=null;
		for(Downline downlineBuscar:ManjeadorDownline.getInstanciar().obtenerLista()){
			String nicknameUser=ManjeadorDownline.getInstanciar().buscarDownlineUser(downlineBuscar.getIdUsuario()).getNickname();
			String edadN=Integer.toString(ManjeadorUsuario.getInstanciar().buscarUsuario(nicknameUser).getEdad());
			if(edadN.contains(edad)==true){
				if(idUser==null){
					idUser=Integer.toString(downlineBuscar.getIdDownline());
				}else{
					idUser=idUser+" "+downlineBuscar.getIdDownline();
				}
			}
		}		
		return idUser;
	}
	/**
		Metodo que busca por el email
		@param String email
		@return String email
	*/
	public static String searchEmail(String email){		
		String idUser=null;
		for(Downline downlineBuscar:ManjeadorDownline.getInstanciar().obtenerLista()){
			String nicknameUser=ManjeadorDownline.getInstanciar().buscarDownlineUser(downlineBuscar.getIdUsuario()).getNickname();
			String emailN=ManjeadorUsuario.getInstanciar().buscarUsuario(nicknameUser).getEmail();
			if(emailN.contains(email)==true){
				if(idUser==null){
					idUser=Integer.toString(downlineBuscar.getIdDownline());
				}else{
					idUser=idUser+" "+downlineBuscar.getIdDownline();
				}
			}
		}		
		return idUser;
	}	
	/**
		Metodo que busca por el email y la edad
		@param String edad
		@param String email
		@return String idUser
	*/
	public static String searchEdadEmail(String edad,String email){
		String idUser=null;
		for(Downline downlineBuscar:ManjeadorDownline.getInstanciar().obtenerLista()){	
			String nicknameUser=ManjeadorDownline.getInstanciar().buscarDownlineUser(downlineBuscar.getIdUsuario()).getNickname();
			String emailN=ManjeadorUsuario.getInstanciar().buscarUsuario(nicknameUser).getEmail();
			if(emailN.contains(email)==true){				
				String edadN=Integer.toString(ManjeadorUsuario.getInstanciar().buscarUsuario(nicknameUser).getEdad());
					if(edadN.contains(edad)==true){
						if(idUser==null){
							idUser=Integer.toString(downlineBuscar.getIdDownline());
						}else{
							idUser=idUser+" "+downlineBuscar.getIdDownline();
						}
					}
			}
		}	
		return idUser;
	}
	/**
		Metodo que busca por el nombre y la edad
		@param String edad
		@param String nombre
		@return String idUser
	*/
	public static String searchNombreEdad(String nombre,String edad){
		String idUser=null;
		for(Downline downlineBuscar:ManjeadorDownline.getInstanciar().obtenerLista()){			
			if(downlineBuscar.getNombre().contains(nombre)==true){
				String nicknameUser=ManjeadorDownline.getInstanciar().buscarDownlineUser(downlineBuscar.getIdUsuario()).getNickname();
				String edadN=Integer.toString(ManjeadorUsuario.getInstanciar().buscarUsuario(nicknameUser).getEdad());
					if(edadN.contains(edad)==true){
						if(idUser==null){
							idUser=Integer.toString(downlineBuscar.getIdDownline());
						}else{
							idUser=idUser+" "+downlineBuscar.getIdDownline();
						}
					}
			}
		}		
		return idUser;
	}
	/**
		Metodo que busca por el email y el nombre
		@param String nombre
		@param String email
		@return String idUser
	*/
	public static String searchNombreEmail(String nombre,String email){
		String idUser=null;
		for(Downline downlineBuscar:ManjeadorDownline.getInstanciar().obtenerLista()){
			if(downlineBuscar.getNombre().contains(nombre)==true){
				String nicknameUser=ManjeadorDownline.getInstanciar().buscarDownlineUser(downlineBuscar.getIdUsuario()).getNickname();
				String emailN=ManjeadorUsuario.getInstanciar().buscarUsuario(nicknameUser).getEmail();
				if(emailN.contains(email)==true){						
					if(idUser==null){
						idUser=Integer.toString(downlineBuscar.getIdDownline());
					}else{
						idUser=idUser+" "+downlineBuscar.getIdDownline();
					}					
				}
			}
		}		
		return idUser;
	}
	/**
		Metodo que busca por el email, el nombre y la edad
		@param String nombre
		@param String edad
		@param String email
		@return String idUser
	*/
	public static String search(String nombre,String email, String edad){
		String idUser=null;
		for(Downline downlineBuscar:ManjeadorDownline.getInstanciar().obtenerLista()){
			if(downlineBuscar.getNombre().contains(nombre)==true){
				String nicknameUser=ManjeadorDownline.getInstanciar().buscarDownlineUser(downlineBuscar.getIdUsuario()).getNickname();
				String emailN=ManjeadorUsuario.getInstanciar().buscarUsuario(nicknameUser).getEmail();
				if(emailN.contains(email)==true){						
					String edadN=Integer.toString(ManjeadorUsuario.getInstanciar().buscarUsuario(nicknameUser).getEdad());
					if(edadN.contains(edad)==true){
						if(idUser==null){
							idUser=Integer.toString(downlineBuscar.getIdDownline());
						}else{
							idUser=idUser+" "+downlineBuscar.getIdDownline();
						}
					}
				}
			}
		}		
		return idUser;
	}
}