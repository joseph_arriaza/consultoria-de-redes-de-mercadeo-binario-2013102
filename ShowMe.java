package org.jarriaza.utilidad;
import org.jarriaza.manejadores.ManjeadorUsuario;
import org.jarriaza.manejadores.ManjeadorDownline;
import org.jarriaza.beans.Downline;
import org.jarriaza.beans.Usuario;
import org.jarriaza.app.AppMiembro;
import java.util.HashMap;
/**
	Clase en donde se muestran las especificaciones de los usuarios
*/
public class ShowMe {
	/**
		Metodo en el que se muestran las especificaciones del usuario logeado
		@param String rol
		@param HashMap<String,String> objeto
	*/
	public static void mostrarMisEspecificaciones(String rol,HashMap<String,String> objeto){
		if(rol.equals("administrador")){
			System.out.println("Rol       :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango());
			System.out.println("Nickname  :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getNickname());
			System.out.println("Password  :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getPass());
			System.out.println("Nombre    :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getNombre());
			System.out.println("Apellido  :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getApellido());
			System.out.println("Edad      :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getEdad());
			System.out.println("E-mail    :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getEmail());
			System.out.println("Direccion :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getDireccion());
		}else{
			Downline downline=ManjeadorDownline.getInstanciar().buscarDownlineUser(ManjeadorUsuario.getInstanciar().usuarioLogeado().getIdUsuario());
			System.out.println("Id	  :      "+downline.getIdDownline());
			System.out.println("Rol       :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango());
			System.out.println("Nickname  :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getNickname());
			System.out.println("Password  :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getPass());
			System.out.println("Nombre    :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getNombre());
			System.out.println("Apellido  :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getApellido());
			System.out.println("Edad      :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getEdad());
			System.out.println("E-mail    :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getEmail());
			System.out.println("Direccion :	"+ManjeadorUsuario.getInstanciar().usuarioLogeado().getDireccion());
			System.out.println("Agregador : 	"+downline.getPersona());
			System.out.println("Tarjeta	  : 	"+downline.getTarjetaCred());
			if(downline.getActivo()==true){
				System.out.println("Activo	  :	Si");
			}else{
				System.out.println("Activo	  :	No");
			}
			if(downline.getLeft()!=null){
				System.out.println("Left	  :	"+downline.getLeft());
			}
			if(downline.getRight()!=null){
				System.out.println("Right	  :	"+downline.getRight());
			}
			System.out.println("Consumo	  :	"+downline.getConsumo());
			if(downline.getPersonasAgregadas()!=null){
				String personas=downline.getPersonasAgregadas();
				String verPersonas[]=personas.split(" ");
				System.out.println("Aregados  :     "+verPersonas.length);
			}
			AppMiembro.mostrarPiernaRight(downline,0,objeto);
			AppMiembro.mostrarPiernaLeft(downline,0,objeto);
			AppMiembro.mostrarPiernaRight(downline,0,objeto);
			if(objeto.get("derecha")==null){
				objeto.put("derecha","0");
			}				
			if(objeto.get("izquierda")==null){
				objeto.put("izquierda","0");
			}
			if(Integer.parseInt(objeto.get("izquierda"))<=Integer.parseInt(objeto.get("derecha"))){
				rango("izquierda",objeto);
			}
			if(Integer.parseInt(objeto.get("izquierda"))>Integer.parseInt(objeto.get("derecha"))){
				rango("derecha",objeto);
			}
		}
	}
	/**
		Metodo en el que se muestran las especificaciones del usuario especifico
		@param String nickname
		@param HashMap<String,String> objeto
	*/
	public static void mostrarEspecificacionesDownline(String nickname,HashMap<String,String> objeto){
		Downline downline=ManjeadorDownline.getInstanciar().buscarDown(nickname);
		Usuario usuario=ManjeadorUsuario.getInstanciar().buscarUsuario(nickname);
		AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDown(nickname),0,objeto);
		AppMiembro.mostrarPiernaLeft(ManjeadorDownline.getInstanciar().buscarDown(nickname),0,objeto);
		AppMiembro.mostrarPiernaRight(ManjeadorDownline.getInstanciar().buscarDown(nickname),0,objeto);
		System.out.println("Rol       :	"+usuario.getRango());
		System.out.println("Nickname  :	"+downline.getNickname());
		System.out.println("Nombre    :	"+downline.getNombre());
		System.out.println("Apellido  :	"+usuario.getApellido());
		System.out.println("Edad      :	"+usuario.getEdad());
		System.out.println("E-mail    :	"+usuario.getEmail());
		System.out.println("Direccion :	"+usuario.getDireccion());
		System.out.println("Agregador : 	"+downline.getPersona());
		System.out.println("Tarjeta	  : 	"+downline.getTarjetaCred());
		if(downline.getActivo()==true){
			System.out.println("Activo	  :	Si");
		}else{
			System.out.println("Activo	  :	No");
		}
		if(downline.getLeft()!=null){
			System.out.println("Left	  :	"+downline.getLeft());
		}
		if(downline.getRight()!=null){
			System.out.println("Right	  :	"+downline.getRight());
		}
		System.out.println("Consumo	  :	"+downline.getConsumo());
		if(downline.getPersonasAgregadas()!=null){
			String personas=downline.getPersonasAgregadas();
			String verPersonas[]=personas.split(" ");
			System.out.println("Aregados  :     "+verPersonas.length);
		}
		if(objeto.get("derecha")==null){
				objeto.put("derecha","0");
		}				
		if(objeto.get("izquierda")==null){
			objeto.put("izquierda","0");
		}
		if(Integer.parseInt(objeto.get("izquierda"))<=Integer.parseInt(objeto.get("derecha"))){
			rango("izquierda",objeto);
		}
		if(Integer.parseInt(objeto.get("izquierda"))>Integer.parseInt(objeto.get("derecha"))){
			rango("derecha",objeto);
		}
	}
	public static void rango(String lado, HashMap<String,String> objeto){
		if(Integer.parseInt(objeto.get(lado))/10<=99){
			System.out.println("Rango     :	Byte");
		}else{
			if(Integer.parseInt(objeto.get(lado))/10<=1023){
				System.out.println("Rango     :	Kilo");
			}else{
				if(Integer.parseInt(objeto.get(lado))/10<=2048){
					System.out.println("Rango     :	Mega");
				}else{
					if(Integer.parseInt(objeto.get(lado))/10<=3071){
						System.out.println("Rango     :	Giga");
					}else{
						if(Integer.parseInt(objeto.get(lado))/10<=4095){
							System.out.println("Rango     :	Tera");
						}
					}
				}
			}
		}
	}
}
