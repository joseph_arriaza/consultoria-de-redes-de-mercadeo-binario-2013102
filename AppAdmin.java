package org.jarriaza.app;
import org.jarriaza.beans.Usuario;
import org.jarriaza.beans.Compra;
import org.jarriaza.beans.Oferta;
import org.jarriaza.utilidad.Comandos;
import org.jarriaza.utilidad.LeerDato;
import org.jarriaza.utilidad.Decodificador;
import org.jarriaza.utilidad.Editar;
import org.jarriaza.utilidad.ShowMe;
import org.jarriaza.utilidad.Fecha;
import org.jarriaza.utilidad.ComandosProductos;
import org.jarriaza.manejadores.ManjeadorUsuario;
import org.jarriaza.manejadores.ManjeadorCompra;
import org.jarriaza.manejadores.ManjeadorDownline;
import org.jarriaza.manejadores.ManjeadorProducto;
import org.jarriaza.manejadores.ManjeadorOferta;
import java.util.HashMap;
/**
	Clase AppAdmin esta clase tiene las funciones del administrador impelementado la interfaz y extendiendose a la clase abstracta para el uso de sus metodos
*/
public class AppAdmin extends AbstractAppRol implements Comandos{
	/**
		Verifica si el correo es valido, si lleva @ y .
		@param String email
	*/
	public static boolean isEmail(String email){
		boolean verificarArroba=false;
		char[] charArray = email.toCharArray();
		for(int i=0;i<charArray.length;i++){
			if(charArray[i]=='@'){
				for(int a=i+1;a<charArray.length;a++){
					if(charArray[a]=='@'){
						return verificarArroba;
					}else{
						if(charArray.length>a+1){
							if(charArray[a+1]=='.'){
								if(charArray.length>a+2){
									int verificarChar=Integer.parseInt(Integer.toString(charArray[a+2]));
									if (verificarChar>=65 && verificarChar<=122){
										if (verificarChar<91||verificarChar>96){
											verificarArroba=true;
											return verificarArroba;
										}else{
											return verificarArroba;
										}
									}else{
										return verificarArroba;
									}
								}
							}
						}
					}
				}
			}
		}
		return verificarArroba;
	}
	/**
		Con este metodo se agrega un escuchador para que pueda realizar las funciones
		@param Decodificador decodificador
	*/
	public AppAdmin(Decodificador decodificador){
		decodificador.addActionListener(this);
		super.setDecodificador(decodificador);
	}
	/**
		Con este metodo se agrega un usuario
		@param Usuario usuario es el usuario a agregar.
	*/
	public void agregarUsuario(Usuario usuario){
		ManjeadorUsuario.getInstanciar().agregarUsuario(usuario);
		System.out.println("	Usuario agregado correctamente.");
	}
	/**
		Con este metodo se elimina un usuario
		@param Sring nickname Elimina el usuario por medio del nickname, ya que lo busca y si lo encuentra lo elimina y si no dice que el usuair no existe
	*/
	public void eliminarUsuario(String nickname){
		int verificacionExistenciaUser=0;
		Usuario usuarioEliminar = ManjeadorUsuario.getInstanciar().buscarUsuario(nickname);
		for(Usuario usuario : ManjeadorUsuario.getInstanciar().obtenerListaUsuario()){
			if(usuario.getRango().equals("administrador")){
				if(usuarioEliminar==usuario){
					if(usuario.getNickname()!=ManjeadorUsuario.getInstanciar().usuarioLogeado().getNickname()){
						verificacionExistenciaUser=1;
						break;
					}else{
						verificacionExistenciaUser=3;
						break;
					}
				}	
			}else{
				verificacionExistenciaUser=4;
			}
		}
		if(verificacionExistenciaUser==0){
			verificacionExistenciaUser=2;
		}
		if(verificacionExistenciaUser==1){
			ManjeadorUsuario.getInstanciar().eliminarUsuario(usuarioEliminar);
			System.out.println("	Usuario eliminado correctamente.");
		}
		if(verificacionExistenciaUser==2){
			System.out.println("	El usuario no existe.");
		}
		if(verificacionExistenciaUser==3){
			System.out.println("	No puede eliminarte a ti mismo.");
		}
		if(verificacionExistenciaUser==4){
			System.out.println("	Solo puedes elimnar administradores.");
		}
	}
	/**
		Con este metodo se listan todos los usuarios y downlines
	*/
	public void listarUsuarios(){
		for(Usuario usuario : ManjeadorUsuario.getInstanciar().obtenerListaUsuario()){
			System.out.println("____________________________________________");
			System.out.println("Nickname: 	"+usuario.getNickname());
			System.out.println("Nombre:	 	"+usuario.getNombre());
			System.out.println("Apellido: 	"+usuario.getApellido());
			System.out.println("Edad: 		"+usuario.getEdad());
			System.out.println("Rol: 		"+usuario.getRango());
			System.out.println("Email: 		"+usuario.getEmail());
			System.out.println("Direccion:	"+usuario.getDireccion());
			System.out.println("____________________________________________");
		}
	}
	
	public void agregarOferta(String producto,String fechaOferta,String oferta){
		int descuento=0;
		if(oferta.equals("2*1")){
			//
		}else{
			descuento=Integer.parseInt(oferta);
			if(descuento!=50){
				descuento=100-descuento;
				oferta=Integer.toString(descuento);
			}
		}
		if(ManjeadorOferta.getInstancia().verificarOferta(producto,fechaOferta)==false){
			Oferta offert=new Oferta(fechaOferta,producto,oferta);
			ManjeadorOferta.getInstancia().agregarOferta(offert);
			System.out.println("	Oferta agregada satisfactoriamente al producto "+producto+" en la fecha "+fechaOferta+" ");
		}else{
			System.out.println("	Ya existe una oferta en esta fehca para estre producto.");
		}
	}
	/**
		Con este metodo se reconoce el comando y si no aparece lo manda a la clase abstracta
		@param String accion
		@param HashMap<String,String> objeto
	*/
	public void avisarAccionar(String accion, HashMap<String, String> objeto){
		switch(accion){
			case "list user":
				this.listarUsuarios();
				break;
			case "search":
					System.out.println("Hola");
				break;
			case "add user":
				if(objeto.get("nickname")!=null && objeto.get("pass")!=null && objeto.get("nombre")!=null && objeto.get("apellido")!=null && objeto.get("edad")!=null && objeto.get("email")!=null && objeto.get("direccion")!=null){
					if(AppAdmin.isEmail(objeto.get("email"))){
						if(ComandosProductos.isNumeric(objeto.get("edad"))){
							System.out.println("Esta seguro que desea agreagar a un administrador con las siguientes propiedades?");
							System.out.println("Nickmame:    "+objeto.get("nickname"));
							System.out.println("Pass:        "+objeto.get("pass"));
							System.out.println("Nombre:      "+objeto.get("nombre"));
							System.out.println("Apellido:    "+objeto.get("apellido"));
							System.out.println("Edad:        "+objeto.get("edad"));
							System.out.println("Email:       "+objeto.get("email"));
							System.out.println("Direccion:   "+objeto.get("direccion"));
							System.out.println("Si o No");
							String op=LeerDato.getInstancia().leer();
							if(op.equalsIgnoreCase("Si")){
								int edad=Integer.parseInt(objeto.get("edad"));
								int id=ManjeadorUsuario.getInstanciar().lastId()+1;
								boolean aceptar=ManjeadorUsuario.getInstanciar().verificarUser(objeto.get("nickname"));
								if(aceptar==false){
									Usuario usuario=new Usuario(id,objeto.get("nickname"),objeto.get("pass"),objeto.get("nombre"),objeto.get("apellido"),"administrador",edad,objeto.get("email"),objeto.get("direccion"));
									this.agregarUsuario(usuario);
								}else{
									System.out.println("	El usuario ya existe.");
								}
							}else{
								System.out.println("	No se ha agregado al usuario.");
							}
						}else{
							System.out.println("	La edad debe de ser tipo numerico.");
						}
					}else{
						System.out.println("	Ingresa un email valido.");
					}	
				}else{
					if(objeto.get("nickname")==null){
						System.out.println("	Debe de ingresar un nickname, nickname=nickname");
					}
					if(objeto.get("pass")==null){
						System.out.println("	Debe ingresar una password, pass=pass");
					}
					if(objeto.get("nombre")==null){
						System.out.println("	Debe ingresar un nombre, nombre=nombre");
					}				
					if(objeto.get("apellido")==null){
						System.out.println("	Debe ingresar un apellido, apellido=apellido");
					}
					if(objeto.get("edad")==null){
						System.out.println("	Debe ingresar una edad, edad=edad");
					}
					if(objeto.get("email")==null){
						System.out.println("	Debe ingresar un email, email=email");
					}
					if(objeto.get("direccion")==null){
						System.out.println("	Debe ingresar una direccion, direccion=direccion");
					}
				}
				break;
			case "remove user":
				if(objeto.get("nickname")!=null){
					this.eliminarUsuario(objeto.get("nickname"));
				}else{
					System.out.println("	Debe ingresar un nickname.");
				}
				break;
			case "add product":
				if(objeto.get("producto")!=null && objeto.get("precio")!=null && objeto.get("cantidad")!=null){
					ComandosProductos.agregarProducto(objeto.get("producto"),objeto.get("precio"),objeto.get("cantidad"));
				}else{
					if(objeto.get("producto")==null){
						System.out.println("	Ingrese un producto,  \"product\"");
					}
					if(objeto.get("precio")==null){
						System.out.println("	Ingrese un precio, precio=123.");
					}
					if(objeto.get("cantidad")==null){
						System.out.println("	Ingrese una cantidad, cantidad=123.");
					}
				}
				break;
			case "remove product":
				if(objeto.get("producto")!=null){
					ComandosProductos.eliminarProductos(objeto.get("producto"));
				}else{
					System.out.println("	La sintaxis del comando es remove product \"producto\"");
				}
				break;
			case "edit product":
				Editar.editarProducto(accion, objeto);
				break;
			case "show sales":
				if(objeto.get("producto")!=null){
					if(ManjeadorProducto.getInstancia().verificarProducto(objeto.get("producto"))==true){
						if(ManjeadorCompra.getInstancia().verComprasProducto(objeto.get("producto"))!=null){
							String verCompra[]=ManjeadorCompra.getInstancia().verComprasProducto(objeto.get("producto")).split(" ");
							System.out.println("____________________________________________");
							for(String recorrerCompra:verCompra){
								int idCompra=Integer.parseInt(recorrerCompra);
								Compra impCompras=ManjeadorCompra.getInstancia().imprimiCompra(idCompra);							
								System.out.println("Nickname     :		"+impCompras.getNickname());
								System.out.println("Producto     :		"+impCompras.getProducto());
								System.out.println("Fecha Pedido :		"+impCompras.getFechaPedido());
								System.out.println("Fecha Entrega:		"+impCompras.getFechaEntrega());
								System.out.println("____________________________________________");

							}
						}else{
							System.out.println("	No hay compras de este producto.");
						}
					}else{
						System.out.println("	Este producto no existe.");
					}
				}else{
					if(ManjeadorCompra.getInstancia().mostrarCompras().size()>=1){
						System.out.println("____________________________________________");
						for(Compra compra:ManjeadorCompra.getInstancia().mostrarCompras()){							
							System.out.println("Comprador    :		"+ManjeadorDownline.getInstanciar().buscarDown(compra.getNickname()).getNombre());
							System.out.println("Producto     :		"+compra.getProducto());
							System.out.println("Fecha Pedido :		"+compra.getFechaPedido());
							System.out.println("Fecha Entrega:		"+compra.getFechaEntrega());
							System.out.println("____________________________________________");
						}
					}else{
						System.out.println("	No hay compras que mostrar.");
					}
				}
				break;
			case "remove offert":
				if(objeto.get("product")!=null && objeto.get("fecha")!=null){
					String [] fecha=objeto.get("fecha").split("/");
					if(fecha.length==3){
						if(fecha[0].length()==2 && fecha[1].length()==2 && fecha[2].length()==4){
							Oferta ofertaEliminar=ManjeadorOferta.getInstancia().verOferta(objeto.get("product").toLowerCase(),objeto.get("fecha"));
							if(ofertaEliminar!=null){
								ManjeadorOferta.getInstancia().eliminarOferta(ofertaEliminar);
								System.out.println("	Oferta eliminada satisfactoriamente.");
							}else{
								System.out.println("	Esta oferta no existe.");
							}
						}else{
							System.out.println("	El formato de la fecha es, xx/xx/xxxx");
						}
					}else{
						System.out.println("	Debe ingresar una fecha valida, DAY/MONTH/YEAR");
					}
				}else{
					if(objeto.get("product")==null){
						System.out.println("	Debe ingresar un producto, product=\"product\"");
					}
					if(objeto.get("fecha")==null){
						System.out.println("	Debe ingresar una fecha, fecha=\"product\"");
					}
				}
			break;
			case "add offert":
				if(objeto.get("product")!=null && objeto.get("oferta")!=null && objeto.get("fecha")!=null){
					if(ManjeadorProducto.getInstancia().verificarProducto(objeto.get("product").toLowerCase())==true){
						if(objeto.get("oferta").equals("2*1")||objeto.get("oferta").contains("% OFF")==true){
							String [] fecha=objeto.get("fecha").split("/");
							if(fecha.length==3){
								if(ComandosProductos.isNumeric(fecha[0])==true && ComandosProductos.isNumeric(fecha[1])==true && ComandosProductos.isNumeric(fecha[2])==true){
									int day=Integer.parseInt(fecha[0]);
									int month=Integer.parseInt(fecha[1]);
									int year=Integer.parseInt(fecha[2]);
									if(fecha[0].length()==2 && day!=00){
										if(fecha[1].length()==2 && month!=00){
											if(fecha[2].length()==4){
												String fechaHoy=Fecha.fechaHoy();
												String [] fechaDeHoy=fechaHoy.split("/");
												int dayHoy=Integer.parseInt(fechaDeHoy[0]);
												int monthHoy=Integer.parseInt(fechaDeHoy[1]);
												int yearHoy=Integer.parseInt(fechaDeHoy[2]);
												if(year>yearHoy){
													if(objeto.get("oferta").contains("% OFF")==true){
														String ofertaPorcentaje[]=objeto.get("oferta").split("%");
														agregarOferta(objeto.get("product").toLowerCase(),objeto.get("fecha"),ofertaPorcentaje[0]);
													}else{
														agregarOferta(objeto.get("product").toLowerCase(),objeto.get("fecha"),objeto.get("oferta"));
													}
												}else{
													if(year==yearHoy){
														if(month>monthHoy){
															if(objeto.get("oferta").contains("% OFF")==true){
																String ofertaPorcentaje[]=objeto.get("oferta").split("%");
																agregarOferta(objeto.get("product").toLowerCase(),objeto.get("fecha"),ofertaPorcentaje[0]);
															}else{
																agregarOferta(objeto.get("product").toLowerCase(),objeto.get("fecha"),objeto.get("oferta"));
															}
														}else{
															if(month==monthHoy){
																if(day>=dayHoy){
																	if(objeto.get("oferta").contains("% OFF")==true){
																		String ofertaPorcentaje[]=objeto.get("oferta").split("%");
																		agregarOferta(objeto.get("product").toLowerCase(),objeto.get("fecha"),ofertaPorcentaje[0]);
																	}else{
																		agregarOferta(objeto.get("product").toLowerCase(),objeto.get("fecha"),objeto.get("oferta"));
																	}
																}else{
																	System.out.println("	Este DAY ya ha pasado.");
																}														
															}else{
																System.out.println("	Este MONTH ya ha pasado.");
															}
														}	
													}else{
														System.out.println("	Este YEAR ya ha pasado.");
													}							
												}
											}else{
												System.out.println("	Ingresa un YEAR valido, xxxx.");
											}
										}else{
											System.out.println("	Ingresa un MONTH valido, xx.");
										}
									}else{
										System.out.println("	Ingresa un DAY valido, xx.");
									}
								}else{
									System.out.println("	El DAY, el MONTH, y el YEAR deben ser numeros.");
								}
							}else{
								System.out.println("	Debes ingresar una fecha valida DAY/MONTH/YEAR");
							}
						}else{
							System.out.println("	Ingresa una oferta valida, recuerda que puedes poner 2*1 o \"30% OFF\"");
						}
					}else{
						System.out.println("	Este producto no existe.");
					}
				}else{
					if(objeto.get("product")==null){
						System.out.println("	Debe ingresar un producto, product=\"product\".");
					}
					if(objeto.get("oferta")==null){
						System.out.println("	Debe ingresar una oferta, oferta=\"ofert\".");
					}
					if(objeto.get("fecha")==null){
						System.out.println("	Debe ingresar una fecha de oferta, fecha=\"fecha\".");
					}
				}
				break;
			case "logout":
				super.setConnected(false);
				break;
			default:
				super.avisarAccionar(accion, objeto);
				break;
		}
	}
}
