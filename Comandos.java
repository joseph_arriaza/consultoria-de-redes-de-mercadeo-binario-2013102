package org.jarriaza.utilidad;
import java.util.HashMap;
/**
	Interfaz que obliga a implementar el metodo avisarAccionar para verificar los comandos
*/
public interface Comandos{
	/**
		Metodo aviscarAccionar que manda el comando
		@param String resultado es el comando resuelto
		@param HashMap<String,String> objeto
	*/
	void avisarAccionar(String resultado, HashMap<String, String> objeto);
}