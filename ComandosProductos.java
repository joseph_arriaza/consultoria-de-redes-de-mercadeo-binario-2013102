package org.jarriaza.utilidad;
import org.jarriaza.manejadores.ManjeadorProducto;
import org.jarriaza.manejadores.ManjeadorUsuario;
import org.jarriaza.beans.Producto;
/**
	Clase ComandosProductos que contiene las funciones de los comandos de productos
*/
public class ComandosProductos {
	/**
		Metodo que verifica si un valor es numerico
		@param String cadena
	*/
	public static boolean isNumeric(String cadena){
		 try {
		  Integer.parseInt(cadena);
		  return true;
		 }catch (NumberFormatException nfe){
		  return false;
		 }
	}
	/**
		Metodo que agrega un producto
		@param String producto nombre dle producto
		@param String precio precio del producto
		@param String cantidad cantidad del producto
	*/
	public static void agregarProducto(String producto, String precio, String cantidad){
		int precioN,cantidadN;
		if(producto!=null && precio!=null && cantidad!=null){
			if(ComandosProductos.isNumeric(precio)==true){
				if(ComandosProductos.isNumeric(cantidad)){				
					System.out.println("Esta a punto de agregar un producto con estas caracteristicas: ");
					System.out.println("Producto:	"+producto);
					System.out.println("Precio:		"+precio);
					System.out.println("Cantidad:	"+cantidad);
					System.out.println("Si o No?");
					String op=LeerDato.getInstancia().leer();
					if(op.equalsIgnoreCase("si")){
						precioN=Integer.parseInt(precio);
						cantidadN=Integer.parseInt(cantidad);
						boolean verificar=ManjeadorProducto.getInstancia().verificarProducto(producto.toLowerCase());
						if(verificar==false){
							int id=ManjeadorProducto.getInstancia().lastId()+1;
							Producto product=new Producto(id,producto,precioN,cantidadN);
							ManjeadorProducto.getInstancia().agregarProducto(product);
							System.out.println("	Producto agregado correctamente.");
						}else{
							System.out.println("	El producto ya existe.");
						}
					}else{
						System.out.println("	No se ha agregado el producto.");
					}
				}else{
					System.out.println("	La cantidad debe de ser de tipo numerico.");
				}
			}else{
				System.out.println("	El precio debe de ser de tipo numerico.");
			}
		}
	}
	/**
		Muestra la lista de productos
	*/
	public static void mostrarProductos(){
		for(Producto producto:ManjeadorProducto.getInstancia().mostrarProducto()){
			System.out.println("______________________________________________");
			System.out.println("Producto:	"+producto.getProducto());
			System.out.println("Precio:		"+producto.getPrecio());
			System.out.println("Cantidad:	"+producto.getCantidad());
			System.out.println("______________________________________________");
		}
		System.out.println("");
	}
	/**
		Elimina un producto
		@param String producto
	*/
	public static void eliminarProductos(String producto){
		char verificar=0;
		Producto productoEliminar=productoEliminar=ManjeadorProducto.getInstancia().buscarProducto(producto);
		for(Producto product:ManjeadorProducto.getInstancia().mostrarProducto()){	
			if(productoEliminar==product){
				if(producto.equalsIgnoreCase("Cereal")){
					verificar=2;
					break;
				}else{
					ManjeadorProducto.getInstancia().eliminarProducto(productoEliminar);
					verificar=1;
					break;
				}
			}
		}
		if(verificar==2){
			System.out.println("	No puedes eliminar el producto base.");
		}
		if(verificar==1){
			System.out.println("	Producto eliminado correctamente.");
		}
		if(verificar==0){
			System.out.println("	El producto no existe.");
		}
	}
}