package org.jarriaza.sistema;
import org.jarriaza.manejadores.ManjeadorUsuario;
import org.jarriaza.utilidad.LeerDato;
import org.jarriaza.utilidad.Comandos;
import org.jarriaza.utilidad.Decodificador;
import org.jarriaza.app.AppAdmin;
import org.jarriaza.app.AbstractAppRol;
import org.jarriaza.app.AppMiembro;
import java.util.HashMap;
/**
	Clase principal encargada de reconocer roles
*/
public class Principal{
	String comandoVale;
	/**
		Metodo iniciar de la clase principal, en esta se lee el usuairo y el nick para iniciar sesion y se entra segun sea el caso
		@param nick Nickname del usuario
		@param password Password del usuario
	*/
	public void iniciar(){
		Decodificador decodificador = new Decodificador();
		AbstractAppRol appRol = null;
		String nick, password, privilegio, comandoValue;
		int valor=0,intentos=0;
		boolean iniciar,verificar=false;
		do{
			System.out.println("___________________________________");
			System.out.println("Ingrese Nick:");
			nick = LeerDato.getInstancia().leer();
			System.out.println("Ingrese Password:");
			password= LeerDato.getInstancia().leer();
			System.out.println("___________________________________");
			iniciar = ManjeadorUsuario.getInstanciar().iniciarSesion(nick, password);
			if(iniciar==true){
				intentos=0;
				System.out.println("Bienvenido "+ManjeadorUsuario.getInstanciar().usuarioLogeado().getNombre()+" "+ManjeadorUsuario.getInstanciar().usuarioLogeado().getApellido());
				privilegio=ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango();
				String comando;
				switch(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango()){
					case "administrador":
						appRol=new AppAdmin(decodificador);
						break;
					case "miembro":
						appRol=new AppMiembro(decodificador);
						break;
					default:
						System.out.println("	Tu rol no existe.");
				}
				appRol.iniciar();
				System.out.println("Sesion cerrada correctamente.");
				System.out.println("");
			}else{
				System.out.println("Sus credenciales son invalidas!");
				System.out.println("");
				if(intentos!=5){
					System.out.println("Le quedan "+(5-intentos)+" intentos antes de cerrar la aplicacion");
				}else{
					System.out.println("Demasiados intentos incorrectos, la aplicacion se ha cerrado.");
				}
				intentos+=1;
			}
		}while(intentos<=5);
		LeerDato.getInstancia().cerrar();	
	}
}
