package org.jarriaza.utilidad;
import java.util.Scanner;
/**
	Clase para leer datos
*/
public class LeerDato{
	private static LeerDato instancia;
	private Scanner scaner;
	/**
		Con este metodo se instancia para leer un dato
	*/
	private LeerDato(){
		scaner=new Scanner(System.in);	
	}
	/**
		Con este metodo se lee el dato
		@return scanner.nextLine()
	*/
	public String leer(){
		return scaner.nextLine();
	}
	/**
		Con este metodo se cierra el lecto
	*/
	public void cerrar(){
		scaner.close();
	}
	/**
		Metodo del patron Singleton
	*/
	public static LeerDato getInstancia(){
		if(instancia==null){
			instancia=new LeerDato();
		}
		return instancia;		
	}
}
