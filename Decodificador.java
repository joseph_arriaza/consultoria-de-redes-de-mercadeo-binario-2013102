package org.jarriaza.utilidad;
import org.jarriaza.manejadores.ManjeadorUsuario;
import java.util.HashMap;
/**
	Clase dedodificador en donde se decodifica el comando
*/
public class Decodificador{
	private Comandos accionar;
	/**
		Imdica que el accion sera nulo
	*/
	public Decodificador(){
		this.accionar=null;
	}
	/**
		Depura las comillas que van en el comando
		@param String comandos[] recibe el array del comando
		@param String accion recibe la accion
		@param HashMap<String,String> objeto
		@return String accion retorna la accion
	*/
	public String depurarComillas(String comandos[], HashMap<String,String> objeto, String accion){
		for(int i=2;i<comandos.length;i++){
			String claveValor[]=comandos[i].split("=");
			if(claveValor.length==2){
				char [] verificandoChar=claveValor[1].toCharArray();
				if(verificandoChar[0]=='"'){
					if(verificandoChar[verificandoChar.length-1]=='"'){								
						String quitarComilla[]=claveValor[1].split("\"");
						if(quitarComilla.length==2){
							objeto.put(claveValor[0].toLowerCase(),quitarComilla[1]);
						}else{
							accion="sintaxis";
							System.out.println("	Debes ingresar un valor en las comillas en las comillas.");
						}
					}else{
						try{									
							String valor=comandos[i]+" "+comandos[i+1];
							String claveValores[]=valor.split("=");
							if(claveValores.length==2){
								char [] verificandoCharValor=claveValores[1].toCharArray();
								if(verificandoCharValor[verificandoCharValor.length-1]=='"'){																			
									String quitarComilla[]=claveValores[1].split("\"");
									if(quitarComilla.length==2){
										objeto.put(claveValor[0].toLowerCase(),quitarComilla[1]);
									}else{
										accion="sintaxis";
										System.out.println("	Debes ingresar un valor en las comillas en las comillas.");
									}						
								}else{
									accion="sintaxis";
								}								
							}
						}catch(ArrayIndexOutOfBoundsException aioobe){
							accion="sintaxis";
						}
					}
				}else{
					if(verificandoChar[verificandoChar.length-1]=='"'){
						accion="sintaxis";
					}else{
						objeto.put(claveValor[0].toLowerCase(),claveValor[1]);
					}
				}
			}else{
				if(accion.equals("add downline")){
					try{
						if(comandos[comandos.length-4].equalsIgnoreCase("down")){
							char [] verificandoCharValor=comandos[comandos.length-3].toCharArray();
							if(verificandoCharValor[verificandoCharValor.length-1]=='"'){																			
								String quitarComilla[]=comandos[comandos.length-3].split("\"");
								objeto.put("down",quitarComilla[1]);
								char [] verificarCharProducto=comandos[comandos.length-2].toCharArray();
								if(verificarCharProducto[verificarCharProducto.length-1]=='"'){	
									String quitarComillaProduct[]=comandos[comandos.length-2].split("\"");
									if(quitarComillaProduct.length==2){
										objeto.put("product",quitarComillaProduct[1]);
										objeto.put("pierna",comandos[comandos.length-1]);
									}else{
										accion="sintaxis";
										System.out.println("	Debes ingresar un valor en las comillas.");
									}										
								}else{
									accion="sintaxis";									
									break;
								}
							}else{
								accion="sintaxis";
								break;
							}
						}else{
							if(comandos[comandos.length-5].equalsIgnoreCase("down")){
								char [] verificandoCharValor=comandos[comandos.length-4].toCharArray();
								if(verificandoCharValor[verificandoCharValor.length-1]=='"'){																			
									String quitarComilla[]=comandos[comandos.length-4].split("\"");
									objeto.put("down",quitarComilla[1]);
									String producto=comandos[comandos.length-3]+" "+comandos[comandos.length-2];	
									char [] verificarCharProducto=producto.toCharArray();	
									if(verificarCharProducto[verificarCharProducto.length-1]=='"'){	
										String quitarComillaProduct[]=producto.split("\"");
										if(quitarComillaProduct.length==2){
											objeto.put("product",quitarComillaProduct[1]);
											objeto.put("pierna",comandos[comandos.length-1]);
										}else{
											accion="sintaxis";
											System.out.println("	Debes ingresar un valor en las comillas.");
										}
									}else{
										accion="sintaxis";
										break;
									}						
								}else{
									accion="sintaxis";
									break;
								}
							}
						}												
					}catch(ArrayIndexOutOfBoundsException aioobe){
						accion="sintaxis";
						break;
					}
				}
				if(accion.equals("show product")){
					if(comandos.length==3){
						char [] verProducto=comandos[comandos.length-1].toCharArray();
						if(verProducto[0]=='"'){
							if(verProducto[verProducto.length-1]=='"'){
								String quitarComillaProduct[]=comandos[comandos.length-1].split("\"");
								if(quitarComillaProduct.length==2){
									objeto.put("product",quitarComillaProduct[1]);
								}else{
									accion="sintaxis";
									System.out.println("	Debes ingresar un valor en las comillas.");
								}
							}
						}else{
							accion="sintaxis";
						}
					}
					if(comandos.length==4){
						String producto=comandos[comandos.length-2]+" "+comandos[comandos.length-1];
						char [] verProducto=producto.toCharArray();
						if(verProducto[0]=='"'){
							if(verProducto[verProducto.length-1]=='"'){
								String quitarComillaProduct[]=producto.split("\"");
								if(quitarComillaProduct.length==2){
									objeto.put("product",quitarComillaProduct[1]);
								}else{
									accion="sintaxis";
									System.out.println("	Debes ingresar un valor en las comillas.");
								}
							}
						}else{
							accion="sintaxis";
						}
					}
				}
				if(accion.equals("add product")){
					if(comandos.length==5){
						char [] verProducto=comandos[comandos.length-3].toCharArray();
						if(verProducto[0]=='"'){
							if(verProducto[verProducto.length-1]=='"'){
								String quitarComillaProduct[]=comandos[comandos.length-3].split("\"");
								if(quitarComillaProduct.length==2){
									objeto.put("producto",quitarComillaProduct[1]);
								}else{
									accion="sintaxis";
									System.out.println("	Debes ingresar un valor en las comillas.");
								}
							}
						}else{
							accion="sintaxis";
						}
					}
					if(comandos.length==6){
						String producto=comandos[comandos.length-4]+" "+comandos[comandos.length-3];
						char [] verProducto=producto.toCharArray();
						if(verProducto[0]=='"'){
							if(verProducto[verProducto.length-1]=='"'){
								String quitarComillaProduct[]=producto.split("\"");
								if(quitarComillaProduct.length==2){
									objeto.put("producto",quitarComillaProduct[1]);
								}else{
									accion="sintaxis";
									System.out.println("	Debes ingresar un valor en las comillas.");
								}
							}
						}else{
							accion="sintaxis";
						}
					}
				}
			}
		}
		return accion;
	}
	/**
		Decodifica el comando para ver su accion
		@param String comando recibe el comando ingresado
	*/
	public void decodificarComando(String comando){
		String[] comandos = comando.split(" ");
		String accion = "";
		HashMap<String, String> objeto = new HashMap<String, String>();		
		if(comandos.length==1){
			accion=comandos[0];
		}
		if(comandos.length>=2){
			accion=comandos[0];
			if(accion.equals("calc")){
				if(comandos.length==2){
					accion="calc";
					objeto.put("calc",comandos[1]);
				}
			}else{
				accion=comandos[0]+" "+comandos[1];
				if(accion.equals("search downline")){
					if(comandos.length<=15){
						accion=depurarComillas(comandos,objeto,accion);
					}else{
						accion="sintaxis";
					}
				}
				if(accion.equals("show product")){
					if(comandos.length>=2 && comandos.length<=4){
						accion=depurarComillas(comandos,objeto,accion);
					}else{
						accion="sintaxis";
					}
				}
				if(ManjeadorUsuario.getInstanciar().usuarioLogeado().getRango().equals("miembro")){
					if(accion.equals("add downline")){
						if(comandos.length<=20){
							accion=depurarComillas(comandos,objeto,accion);
						}else{
							accion="sintaxis";
						}
					}else{
						if(accion.equals("edit me")){
							if(comandos.length<=14){
								accion=depurarComillas(comandos,objeto,accion);
							}else{
								accion="sintaxis";
							}
						}else{
							accion=DepurarComillas.depurar(comando,objeto);
						}
					}
				}else{
					if(accion.equals("list user")){
						accion=comandos[0]+" "+comandos[1];
					}else{
						if(accion.equals("add user")){
							if(comandos.length<=14){
								accion=depurarComillas(comandos,objeto,accion);
							}else{
								accion="sintaxis";
							}
						}else{
							if(accion.equals("add product")){
								if(comandos.length<=6){
									accion=depurarComillas(comandos,objeto,accion);
								}else{	
									accion="sintaxis";
								}
							}else{
								if(accion.equals("edit product")){
									if(comandos.length<=6){
										accion=depurarComillas(comandos,objeto,accion);
									}
								}else{
									if(accion.equals("add offert")){
										if(comandos.length<=6){
											accion=depurarComillas(comandos,objeto,accion);
										}
									}else{
										if(accion.equals("remove offert")){
											if(comandos.length<=6){
												accion=depurarComillas(comandos,objeto,accion);
											}
										}else{
											accion=DepurarComillas.depurar(comando,objeto);	
										}
									}
								}
							}
						}
					}				
				}	
			}
		}
		if(comandos.length>=3){
			if(comandos[0].equals("search") && comandos[1].equals("downline")){
				accion=comandos[0]+" "+comandos[1];
				for(int posicion=2;posicion<comandos.length;posicion++){
					String claveValor[] = comandos[posicion].split("=");
					if(claveValor.length==2){
						objeto.put(claveValor[0].toLowerCase(),claveValor[1]);
					}
				}
			}
			if(comandos[0].equals("list") && comandos[1].equals("downlines")){
				accion=comandos[0]+" "+comandos[1]+" down";
				if(comandos[2].equals("right")||comandos[2].equalsIgnoreCase("left")){
					accion=comandos[0]+" "+comandos[1]+" "+comandos[2].toLowerCase();
				}else{
					char [] charArray=comandos[2].toCharArray();
					if(charArray[0]=='"'){
						for(int cont=2;cont<charArray.length;cont++){
							if(charArray[cont]=='"'){
								if(comandos.length==3){
									String [] depurarComilla=comandos[2].split("\"");
									objeto.put("down",depurarComilla[1]);
								}else{
									if(comandos.length==4){
										if(comandos[3].equalsIgnoreCase("right")||comandos[3].equalsIgnoreCase("left")){										
											accion="list downlines down "+comandos[3].toLowerCase();
											String [] depurarComilla=comandos[2].split("\"");
											objeto.put("down",depurarComilla[1]);
											objeto.put("pierna",comandos[3].toLowerCase());
										}else{
											accion="sintaxis";
											System.out.println("	La sintaxis del comando es list downline \"idDownline\" left or right");
										}
									}else{
										accion="sintaxis";
										System.out.println("	La sintaxis del comando es list downline \"idDownline\"");
									}
								}
							}
						}
					}else{
						accion="sintaxis";
						System.out.println("	La sintaxis del comando es list downline \"idDownline\"");
					}
				}	
			}
		}		
		if(accionar!=null){
			accionar.avisarAccionar(accion, objeto);
		}
	}
	/**
		Agrega un escuchador
	*/
	public void addActionListener(Comandos interfaz){
		this.accionar=interfaz;
	}
	
}